﻿using System.Collections.Generic;

using UnityEngine;

public class Tmp_NoiseExperiment : MonoBehaviour
{
    [SerializeField] private Transform ringPrefab;

    [SerializeField] private float maxY = 10;
    [SerializeField] private float distanceBetweenRings = 2;
    [SerializeField] private int ringNumber = 10;

    [SerializeField] private float noiseCoeffMultiplier = 1;


    private List<Transform> rings = new List<Transform>();

    private float prevMaxY;
    private float prevDistanceBetweenRings;
    private int prevRingNumber;
    private float prevNoiseCoeffMultiplier;


    private void Start()
    {
        SpawnRings();
    }

    private void Update()
    {
        if (CheckValues())
        {
            SpawnRings();
        }
    }

    private void SpawnRings()
    {
        for (var i = rings.Count; i < ringNumber; i++)
        {
            var ring = Instantiate(ringPrefab);
            ring.forward = Vector3.right;
            rings.Add(ring);
        }

        PlaceRings();
    }

    private void PlaceRings()
    {
        var firstX = -(distanceBetweenRings * (ringNumber - 1) / 2);

        for (var ring = 0; ring < ringNumber; ring++)
        {
            var x = firstX + distanceBetweenRings * ring;

            var rawNoiseCoeff = ring / (ringNumber - 1f);
            var noiseCoeff = rawNoiseCoeff * noiseCoeffMultiplier;
            var noise = Mathf.PerlinNoise(noiseCoeff, noiseCoeff);
            var y = -maxY + noise * 2 * maxY;

            Debug.Log($"ring: {ring} | rawCoeff: {rawNoiseCoeff:F3} | coeff: {noiseCoeff:F3} | noise: {noise:F3} -> y: {y,6:F3}");

            rings[ring].position = new Vector3(x, y, 0);
        }

        UpdateValues();
    }

    private void UpdateValues()
    {
        prevDistanceBetweenRings = distanceBetweenRings;
        prevMaxY = maxY;
        prevNoiseCoeffMultiplier = noiseCoeffMultiplier;
        prevRingNumber = ringNumber;
    }

    private bool CheckValues()
        => prevRingNumber != ringNumber
        || prevNoiseCoeffMultiplier != noiseCoeffMultiplier
        || prevMaxY != maxY
        || prevDistanceBetweenRings != distanceBetweenRings;
}
