﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Utils;

public class tmp : MonoBehaviour
{
    private void Start()
    {
        var colors = new Color[] { Color.blue, Color.red, Color.yellow };
        for (var power = 2; power <= 6; power++)
        {
            var samples = 20;

            DrawCurve(colors[0], power, samples, Easings.EaseIn);
            DrawCurve(colors[1], power, samples, Easings.EaseOut);
            DrawCurve(colors[2], power, samples, Easings.EaseInOut);
        }
    }

    private static void DrawCurve(Color color, int power, int samples, Func<int, float, float> func)
    {
        var prevPoint = new Vector2(0 + power - 2, 0);
        for (float s = 1; s <= samples; s++)
        {
            var x = s / samples;
            var y = func(power, x);
            var point = new Vector2(x + power - 2, y);
            Debug.DrawLine(prevPoint, point, color, Mathf.Infinity);
            prevPoint = point;
        }
    }
}