﻿using UnityEngine;

public class Helper : MonoBehaviour
{
    [SerializeField] private float timeScale = 1f;


    private void Update()
    {
        Time.timeScale = timeScale;
    }
}
