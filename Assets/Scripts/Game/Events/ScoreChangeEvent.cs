﻿namespace Events
{
    namespace Game
    {
        public class ScoreChangeEvent
        {
            public readonly int NewScore;

            public ScoreChangeEvent(int newScore) => NewScore = newScore;
        }
    }
}