﻿namespace Events.Game
{
    class PlayerMovedEvent
    {
        public float FractionalPosition => AbsolutePosition / AbsolutePathLength;
        public readonly float AbsolutePosition;
        public readonly float AbsolutePathLength;

        public PlayerMovedEvent(float absolutePosition, float absolutePathLength)
        {
            AbsolutePosition = absolutePosition;
            AbsolutePathLength = absolutePathLength;
        }


        public static PlayerMovedEvent NoFractionalPosition(float absolutePosition, float absolutePathLength)
        {
            return new PlayerMovedEvent(absolutePosition, absolutePathLength);
        }

        public static PlayerMovedEvent NoAbsolutePosition(float fractionalPosition, float absolutePathLength)
        {
            return new PlayerMovedEvent(fractionalPosition * absolutePathLength, absolutePathLength);
        }

        public static PlayerMovedEvent NoPathLength(float fractionalPosition, float absolutePosition)
        {
            return new PlayerMovedEvent(absolutePosition, absolutePosition / fractionalPosition);
        }
    }
}