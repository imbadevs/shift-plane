﻿namespace Events.Game
{
    class NewCurveStartedEvent
    {
        public readonly float AngleBetweenStartAndEndForwards;

        public NewCurveStartedEvent(float angleBetweenStartAndEndForwards)
        {
            AngleBetweenStartAndEndForwards = angleBetweenStartAndEndForwards;
        }
    }
}
