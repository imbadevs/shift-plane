﻿namespace Events.Game
{
    class LevelFinishedEvent
    {
        public readonly int FinalScore;

        public LevelFinishedEvent(int finalScore)
        {
            FinalScore = finalScore;
        }
    }
}
