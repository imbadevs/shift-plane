﻿using UnityEngine;
using Curves;


public class Path : ILineRenderable
{
    private readonly Spline spline;
    private readonly float[] milestones;
    private readonly int segmentsPerCurve;


    public readonly float Length;
    public readonly int PointsNumber;
    public float GetPointMilestone(int pointIndex) => milestones[pointIndex * segmentsPerCurve];


    public Path(Spline spline, int segmentsPerCurve)
    {
        this.spline = spline;
        this.segmentsPerCurve = segmentsPerCurve;
        PointsNumber = spline.CurveNumber + 1;

        milestones = new float[spline.CurveNumber * segmentsPerCurve + 1];
        Length = ComputeLengthAndMilestones();
    }


    private float ComputeLengthAndMilestones()
    {
        var prevPosition = spline.InterpolatePosition(0f);
        var highestMilestoneIndex = milestones.LastIndex();
        for (var milestone = 1; milestone <= highestMilestoneIndex; milestone++)
        {
            var currentPosition = spline.InterpolatePosition((float)milestone / highestMilestoneIndex);
            milestones[milestone] = milestones[milestone - 1] + (currentPosition - prevPosition).magnitude;
            prevPosition = currentPosition;
        }
        return milestones.Last();
    }

    private int FindHighestNotGreaterThan(float predicate)
    {
        var begin = 0;
        var end = milestones.Length - 2;
        var i = milestones.Length / 2;

        while (!(milestones[i] <= predicate && predicate <= milestones[i + 1]))
        {
            if (milestones[i] < predicate)
            {
                begin = i + 1;
            }
            else
            {
                end = i - 1;
            }
            i = begin + (end - begin + 1) / 2;
        }

        return i;
    }


    public MapPoint FollowPath(float movedDistance)
    {
        movedDistance = Mathf.Clamp(movedDistance, 0f, Length);
        var milestoneIndex = FindHighestNotGreaterThan(movedDistance);
        var extra = (movedDistance - milestones[milestoneIndex]) / (milestones[milestoneIndex + 1] - milestones[milestoneIndex]);
        var state = (milestoneIndex + extra) / (milestones.Length - 1);
        //Debug.Log($"distance: {movedDistance} | milestone[{milestoneIndex}] = {milestones[milestoneIndex]} | extra: {extra} | state: {state} -> {spline.Interpolate(state)}");
        return spline.Interpolate(state);
    }


    public int TotalVertexNumber(int sampleSegments)
    {
        return spline.TotalVertexNumber(sampleSegments);
    }

    public float ApproximateLineWithLength(int sampleSegments, ref Vector3[] vertices, int startIndex)
    {
        return spline.ApproximateLineWithLength(sampleSegments, ref vertices, startIndex);
    }

    public float ApproximateLength(int sampleSegments)
    {
        return spline.ApproximateLength(sampleSegments);
    }

    public void ApproximateLine(int sampleSegments, ref Vector3[] vertices, int startIndex)
    {
        spline.ApproximateLine(sampleSegments, ref vertices, startIndex);
    }

}