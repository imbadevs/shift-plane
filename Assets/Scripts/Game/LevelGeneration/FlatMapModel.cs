﻿using UnityEngine;
using Curves;


/// <summary>
/// Abstract model of game map used to generate actual map and move plane through it.
/// </summary>
class FlatMapModel
{
    /// <summary> Total length of map path. </summary>
    public readonly float Length;

    /// <summary> Bezier curves represent map path. </summary>
    public readonly ICurve[] curves;

    /// <summary>
    /// Milestones divide map path into segments and represent length of path so far at each segment.
    /// Last milestone equals <c>Length</c>.
    /// </summary>
    public readonly float[] milestones;

    public const int samplesPerCurve = 100;

    /// <summary>
    /// Generates random <c>MapModel</c> using <c>MapModelGenerationData</c> as config.
    /// </summary>
    /// <param name="genData">Config for random generation.</param>
    public static FlatMapModel GenerateRandom(PathPointsGenerationData genData)
    {
        var pointsNumber = genData.PointsNumberRange.Random();
        var lastPoint = new MapPoint(Vector3.zero, Vector3.forward);
        var points = new MapPoint[pointsNumber + 1];
        points[0] = lastPoint;

        var angularOffsetSum = 0f;
        var angularOffsetSegmentSum = 0f;

        for (var currentPoint = 1; currentPoint <= pointsNumber; currentPoint++)
        {
            var distance = genData.DistanceBetweenPointsRange.Random();
            var angularOffset = Random.Range(-genData.MaxHorizontalAngle, genData.MaxHorizontalAngle);

            if (Mathf.Abs(angularOffsetSum + 2 * angularOffset) >= genData.MaxHorizontalAngleSum || Mathf.Abs(angularOffsetSegmentSum + 2 * angularOffset) >= genData.MaxHorizontalAngleSum)
            {
                angularOffset *= -1;
                angularOffsetSegmentSum = 0;
            }
            angularOffsetSegmentSum += 2 * angularOffset;
            angularOffsetSum += 2 * angularOffset;

            var rotation = angularOffset;

            var rotatedForward = Quaternion.AngleAxis(angularOffset, Vector3.up) * lastPoint.Forward;
            var position = lastPoint.Position + rotatedForward * distance;
            var forward = Quaternion.AngleAxis(rotation, Vector3.up) * rotatedForward;

            var point = new MapPoint(position, forward);

            points[currentPoint] = point;
            lastPoint = point;
        }

        return new FlatMapModel(points);
    }

    /// <summary>
    /// Generates <c>MapModel</c> using given <paramref name="points"/> to create map path.
    /// </summary>
    /// <param name="points">Path will go through these points.</param>
    public FlatMapModel(MapPoint[] points)
    {
        //curves = new BezierCurve[points.Length - 1];
        //milestones = new float[curves.Length * samplesPerCurve + 1];

        //Vector3 prevSamplePoint = points[0].Position;
        //float prevTotalLength = 0;

        //for (var curveInd = 0; curveInd < curves.Length; curveInd++)
        //{
        //    var curve = new BezierCurve(points[curveInd], points[curveInd + 1]);
        //    curves[curveInd] = curve;

        //    for (var sample = 1; sample <= samplesPerCurve; sample++)
        //    {
        //        var currentSamplePoint = curve.FollowCurve((float)sample / samplesPerCurve).Position;
        //        var currentTotalLength = prevTotalLength + Vector3.Distance(currentSamplePoint, prevSamplePoint);

        //        milestones[curveInd * samplesPerCurve + sample] = currentTotalLength;

        //        prevSamplePoint = currentSamplePoint;
        //        prevTotalLength = currentTotalLength;
        //    }
        //}

        //Length = prevTotalLength;
    }

    public string Show()
    {
        return $"MapModel(Distance: {Length} | Curves: {curves.Length} | Milestones: {milestones.Length})";
    }
}


class MapModelFollower
{
    private readonly FlatMapModel map;

    private readonly bool notifyAboutCurves;
    private int previousCurve = -1;

    public MapModelFollower(FlatMapModel map, bool notifyAboutCurves = false)
    {
        this.map = map;
        this.notifyAboutCurves = notifyAboutCurves;
    }

    /// <summary>
    /// Computes position and heading direction as something is moving along the path.
    /// </summary>
    /// <param name="movedDistance">Distance from start of the path. Clamped within [0, <c>Length</c>]. </param>
    public MapPoint ComputeCurrentPoint(float movedDistance)
    {
        movedDistance = Mathf.Clamp(movedDistance, 0, map.Length);
        var currentMilestoneIndex = FindHighestNotGreaterThan(map.milestones, movedDistance);

        var milestoneExcess = movedDistance - map.milestones[currentMilestoneIndex];
        var sampleLength = map.milestones[currentMilestoneIndex + 1] - map.milestones[currentMilestoneIndex];
        var excessCompensation = milestoneExcess / sampleLength;

        var currentCurve = currentMilestoneIndex / FlatMapModel.samplesPerCurve;
        var state = ((currentMilestoneIndex % FlatMapModel.samplesPerCurve) + excessCompensation) / FlatMapModel.samplesPerCurve;

        var mapPoint = map.curves[currentCurve].Interpolate(state);

        if (notifyAboutCurves)
        {
            CheckNewCurveStart(currentCurve);
            previousCurve = currentCurve;
        }

        return mapPoint;
    }


    private int FindHighestNotGreaterThan(float[] sortedXs, float predicate)
    {
        var begin = 0;
        var end = sortedXs.Length - 2;
        var i = sortedXs.Length / 2;

        while (!(sortedXs[i] <= predicate && predicate <= sortedXs[i + 1]))
        {
            if (sortedXs[i] < predicate)
            {
                begin = i + 1;
            }
            else
            {
                end = i - 1;
            }
            i = begin + (end - begin + 1) / 2;
        }

        return i;
    }


    private void CheckNewCurveStart(int currentCurve)
    {
        if (currentCurve != previousCurve)
        {
            var startForward = map.curves[currentCurve].Start.Forward;
            var endForward = map.curves[currentCurve].End.Forward;

            var deltaAngle = Vector3.SignedAngle(startForward, endForward, Vector3.up);

            GlobalEventSystem.EventDispatcher.Instance.Dispatch(new Events.Game.NewCurveStartedEvent(deltaAngle));
        }
    }
}
