﻿using GlobalEventSystem;

using UnityEngine;

static class PathPointsGenerator
{
    public static MapPoint[] GenerateRandom(PathPointsGenerationData genData)
    {
        var pointsNumber = genData.PointsNumberRange.Random();
        var points = new MapPoint[pointsNumber + 1];
        var prevPoint = new MapPoint(Vector3.zero, Vector3.forward);
        points[0] = prevPoint;

        var horizontalAngleSum = 0f;
        var horizontalAngleSumAtSegment = 0f;

        for (var currentPoint = 1; currentPoint <= pointsNumber; currentPoint++)
        {
            var distance = genData.DistanceBetweenPointsRange.Random();
            var horizontalAngle = Random.Range(-genData.MaxHorizontalAngle, genData.MaxHorizontalAngle);
            var rotation = horizontalAngle;

            if (Mathf.Abs(horizontalAngleSum + horizontalAngle + rotation) >= genData.MaxHorizontalAngleSum || Mathf.Abs(horizontalAngleSumAtSegment + horizontalAngle + rotation) >= genData.MaxHorizontalAngleSum)
            {
                horizontalAngle *= -1;
                horizontalAngleSumAtSegment = 0;
            }
            horizontalAngleSumAtSegment += horizontalAngle + rotation;
            horizontalAngleSum += horizontalAngle + rotation;


            var offsetDirection = Quaternion.AngleAxis(horizontalAngle, Vector3.up) * prevPoint.Forward;
            var position = prevPoint.Position + offsetDirection * distance;
            var forward = Quaternion.AngleAxis(rotation, Vector3.up) * offsetDirection;

            var verticalAngle = Random.Range(-genData.MaxVerticalAngle, genData.MaxVerticalAngle);
            var tan = Mathf.Tan(Mathf.Deg2Rad * verticalAngle);
            var yDelta = distance * tan;
            var pointY = genData.PointYRange.Clamp(prevPoint.Position.y + yDelta);

            var point = new MapPoint(position.With(y: pointY), forward);

            points[currentPoint] = point;
            prevPoint = point;
        }

        EventDispatcher.Instance.Dispatch(new Events.LevelGeneration.MapHeightDeterminedEvent(genData.PointYRange.Min - 1.5f, genData.PointYRange.Max + 1.5f));

        return points;
    }
}