﻿using GlobalEventSystem;

using UnityEngine;


public class LevelGenerationConfigsCreator : MonoBehaviour
{
    [Header("Path Points")]
    [SerializeField] private PathPointsGenerationDataConfiguration pathPointsDataConfiguration;

    [Header("Props Placement")]
    [SerializeField] private PropsPlacementDataConfiguration propsPlacementDataConfiguration;

    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.Storage.LevelLoadedEvent>(GenerateConfig);
    }

    private void GenerateConfig(Events.Storage.LevelLoadedEvent obj)
    {
        PropsPlacementData mapGenerationData = propsPlacementDataConfiguration.CreateMapGenData(obj.LoadedLevel);
        PathPointsGenerationData mapModelGenerationData = pathPointsDataConfiguration.CreateMapModelGenData(obj.LoadedLevel);

        EventDispatcher.Instance.Dispatch
            (new Events.LevelGeneration.LevelGenerationConfigsCreatedEvent(mapModelGenerationData, mapGenerationData));
    }
}

[System.Serializable]
internal class PathPointsGenerationDataConfiguration
{
    [SerializeField] private IntRangeValueFromLevelCalculator pointsNumberRangeCalc;
    [SerializeField] private FloatRangeValueFromLevelCalculator distanceBetweenPointsRangeCalc;
    [SerializeField] private FloatRangeValueFromLevelCalculator pointYRangeCalc;
    [SerializeField] private FloatValueFromLevelCalculator maxVerticalAngleCalc;
    [SerializeField] private FloatValueFromLevelCalculator maxHorizontalAngleCalc;
    [SerializeField] private FloatValueFromLevelCalculator maxHorizontalAngleSumCalc;

    public PathPointsGenerationData CreateMapModelGenData(int level)
    {
        return new PathPointsGenerationData(
            pointsNumberRangeCalc.Calculate(level),
            distanceBetweenPointsRangeCalc.Calculate(level),
            pointYRangeCalc.Calculate(level),
            maxVerticalAngleCalc.Calculate(level),
            maxHorizontalAngleCalc.Calculate(level),
            maxHorizontalAngleSumCalc.Calculate(level)
            );
    }
}

[System.Serializable]
internal class PropsPlacementDataConfiguration
{
    [Header("Rings")]
    [SerializeField] private GameObject ringPrefab;

    [Header("Clouds")]
    [SerializeField] private FloatRangeValueFromLevelCalculator cloudScaleRangeCalc;
    [SerializeField] private IntRangeValueFromLevelCalculator segmentCloudNumberRangeCalc;
    [SerializeField] private FloatRangeValueFromLevelCalculator cloudToPathDistanceRangeCalc;
    [SerializeField] private Renderer[] cloudPrefabs;

    public PropsPlacementData CreateMapGenData(int level)
    {
        return new PropsPlacementData(
            ringPrefab,
            cloudScaleRangeCalc.Calculate(level),
            segmentCloudNumberRangeCalc.Calculate(level),
            cloudToPathDistanceRangeCalc.Calculate(level),
            cloudPrefabs
            );
    }
}