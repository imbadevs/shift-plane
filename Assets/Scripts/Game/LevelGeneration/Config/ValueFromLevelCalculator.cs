﻿using UnityEngine;

[System.Serializable]
abstract class ValueFromLevelCalculator<T>
{
    [SerializeField] protected T baseValue;

    [SerializeField] protected bool dependsOnLevel;
    [SerializeField] protected float multiplier;

    public abstract T Calculate(int level);
}


[System.Serializable]
class IntValueFromLevelCalculator : ValueFromLevelCalculator<int>
{
    public override int Calculate(int level)
    {
        return baseValue + Mathf.RoundToInt(baseValue * (dependsOnLevel ? multiplier * level : 0));
    }
}

[System.Serializable]
class FloatValueFromLevelCalculator : ValueFromLevelCalculator<float>
{
    public override float Calculate(int level)
    {
        return baseValue + baseValue * (dependsOnLevel ? multiplier * level : 0);
    }
}

[System.Serializable]
class FloatRangeValueFromLevelCalculator : ValueFromLevelCalculator<FloatRange>
{
    public override FloatRange Calculate(int level)
    {
        return (FloatRange)baseValue.Sum(baseValue.MultipliedBy(dependsOnLevel ? multiplier * level : 0));
    }
}

[System.Serializable]
class IntRangeValueFromLevelCalculator : ValueFromLevelCalculator<IntRange>
{
    public override IntRange Calculate(int level)
    {
        return (IntRange)baseValue.Sum(baseValue.MultipliedBy(dependsOnLevel ? multiplier * level : 0));
    }
}