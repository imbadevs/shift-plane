﻿using UnityEngine;


/// <summary>
/// Config for random props placement.
/// </summary>
[System.Serializable]
class PropsPlacementData
{
    [SerializeField] private GameObject ringPrefab;

    [SerializeField] private FloatRange cloudScaleRange;
    [SerializeField] private IntRange segmentCloudNumberRange;
    [SerializeField] private FloatRange cloudToPathDistanceRange;
    [SerializeField] private Renderer[] cloudPrefabs;


    public PropsPlacementData(GameObject ringPrefab, FloatRange cloudScaleRange, IntRange segmentCloudNumberRange, FloatRange cloudToPathDistanceRange, Renderer[] cloudPrefabs)
    {
        this.ringPrefab = ringPrefab;
        this.cloudScaleRange = cloudScaleRange;
        this.segmentCloudNumberRange = segmentCloudNumberRange;
        this.cloudToPathDistanceRange = cloudToPathDistanceRange;
        this.cloudPrefabs = cloudPrefabs;
    }


    /// <summary> <c>GameObject</c> to use as rings on map points. </summary>
    public GameObject RingPrefab { get => ringPrefab; }

    /// <summary> Cloud scale range. </summary>
    public FloatRange CloudScaleRange { get => cloudScaleRange; }

    /// <summary> Range of possible number of clouds per path segment. </summary>
    public IntRange SegmentCloudNumberRange { get => segmentCloudNumberRange; }

    /// <summary> Possible distance between cloud and some point on the path.  </summary>
    public FloatRange CloudToPathDistanceRange { get => cloudToPathDistanceRange; }

    /// <summary> Objects that can be used as clouds. </summary>
    public Renderer[] CloudPrefabs { get => cloudPrefabs; }
}
