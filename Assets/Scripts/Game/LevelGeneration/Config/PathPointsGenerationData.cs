﻿using UnityEngine;

/// <summary>
/// Config for random path points generation.
/// </summary>
[System.Serializable]
internal class PathPointsGenerationData
{
    [SerializeField] private IntRange pointsNumberRange;

    [SerializeField] private FloatRange distanceBetweenPointsRange;

    [SerializeField] private FloatRange ringYRange;
    [SerializeField] private float maxVerticalAngle;

    [SerializeField] private float maxHorizontalAngle;
    [SerializeField] private float maxHorizontalAngleSum;

    public PathPointsGenerationData(IntRange pointsNumberRange, FloatRange distanceBetweenPointsRange, FloatRange ringYRange, float maxVerticalAngle, float maxHorizontalAngle, float maxHorizontalAngleSum)
    {
        this.pointsNumberRange = pointsNumberRange;
        this.distanceBetweenPointsRange = distanceBetweenPointsRange;
        this.ringYRange = ringYRange;
        this.maxVerticalAngle = maxVerticalAngle;
        this.maxHorizontalAngle = maxHorizontalAngle;
        this.maxHorizontalAngleSum = maxHorizontalAngleSum;
    }

    /// <summary> Possible number of points. </summary>
    public IntRange PointsNumberRange { get => pointsNumberRange; }

    /// <summary> Possible distance between points (measured only projection on horizontal plane). </summary>
    public FloatRange DistanceBetweenPointsRange { get => distanceBetweenPointsRange; }

    /// <summary> Possible y-coordinate of points. </summary>
    public FloatRange PointYRange { get => ringYRange; }

    /// <summary> Max angle between (horizontal plane) and (vector between <c>points[i].Position</c> and <c>points[i+1].Position</c>). </summary>
    public float MaxVerticalAngle { get => maxVerticalAngle; }

    /// <summary> Max angle between (plane formed by vectors <c>points[i].Forward</c> and <c>Vector3.up</c>)
    /// and (vector between <c>points[i].Position</c> and <c>points[i+1].Position</c>). </summary>
    public float MaxHorizontalAngle { get => maxHorizontalAngle; }

    /// <summary> Used to prevent path loops. </summary>
    public float MaxHorizontalAngleSum { get => maxHorizontalAngleSum; }
}