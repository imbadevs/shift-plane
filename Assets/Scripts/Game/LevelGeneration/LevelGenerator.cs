﻿using Curves;
using GlobalEventSystem;

using UnityEngine;

using static Utils.Random;


/// <summary>
/// Generates path for level and places props using <c>PathPointsGenerationData</c> and <c>PropsPlacementData</c>.
/// </summary>
class LevelGenerator : MonoBehaviour
{
    [SerializeField] private PlayerPathFollower player;
    [SerializeField] private float speedDivider = 2f;
    [SerializeField] private GuideLineDrawer guideLineDrawer;

    private const int SegmentsPerCurve = 100;


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.LevelGeneration.LevelGenerationConfigsCreatedEvent>(GenerateLevel);
    }

    public void GenerateLevel(Events.LevelGeneration.LevelGenerationConfigsCreatedEvent data)
    {
        var splineGenerator = SplineGenerators.CubicBezierWithSpeed(player.Speed / speedDivider);

        var pathPoints = PathPointsGenerator.GenerateRandom(data.PathPointsGenerationData);
        var spline = splineGenerator.NewSpline(pathPoints);
        var path = new Path(spline, SegmentsPerCurve);
        PlaceProps(path, pathPoints, data.PropsPlacementData);
        EventDispatcher.Instance.Dispatch(new Events.LevelGeneration.PathCreatedEvent(path));

        var flatenedPathPoints = pathPoints.Map(point => point.NewWithPosition(y: 0));
        var flatenedSpline = splineGenerator.NewSpline(flatenedPathPoints);
        var planePath = new Path(flatenedSpline, SegmentsPerCurve);
        EventDispatcher.Instance.Dispatch(new Events.LevelGeneration.PlanePathCreatedEvent(planePath));
    }


    private void PlaceProps(Path path, MapPoint[] ringPoints, PropsPlacementData data)
    {
        var ringRoot = new GameObject("Rings").transform;
        var cloudRoot = new GameObject("Clouds").transform;

        var ringRadius = data.RingPrefab.GetComponentInChildren<Renderer>().bounds.extents.y;

        // Place rings on points.
        for (var pointIndex = 1; pointIndex < ringPoints.Length; pointIndex++)
        {
            var ringPosition = ringPoints[pointIndex].Position;
            var ringForward = ringPoints[pointIndex].Forward;
            var ringForwardRotation = Quaternion.LookRotation(ringForward);

            Instantiate(data.RingPrefab, ringPosition, ringForwardRotation, ringRoot).name = "Ring " + pointIndex;

            // Place clouds around ring.
            const int maxRingCloudsNumber = 4;
            var cloudNumber = Random.Range(1, maxRingCloudsNumber + 1);

            for (var cloudIndex = 0; cloudIndex < cloudNumber; cloudIndex++)
            {
                var cloudPrefab = data.CloudPrefabs.PickRandom();
                var scale = data.CloudScaleRange.Random();
                var scaledCloudExtents = cloudPrefab.bounds.extents * scale;

                var deltaAngle = 360f / cloudNumber;
                var angle = Random.Range(cloudIndex * deltaAngle + deltaAngle / 2, (cloudIndex + 1) * deltaAngle - deltaAngle / 2);
                var offsetDirection = Quaternion.AngleAxis(angle, ringForward) * Vector3.up;
                var tmpOffset = offsetDirection * (ringRadius + scaledCloudExtents.magnitude);

                var cloud = Instantiate(cloudPrefab, ringPosition + tmpOffset, ringForwardRotation, cloudRoot);
                cloud.transform.localScale *= scale;

                var bounds = cloud.bounds;
                var closestPoint = bounds.ClosestPoint(ringPosition);
                var cloudRadius = (closestPoint - bounds.center).magnitude;
                var offset = offsetDirection * (ringRadius + cloudRadius);

                cloud.transform.position = ringPosition + offset;
            }
        }

        EventDispatcher.Instance.Dispatch(new Events.LevelGeneration.AllRewardsPlacedEvent(ringPoints.Length - 1));

        // Place clouds around path segments.
        for (var curveIndex = 1; curveIndex < path.PointsNumber - 1; curveIndex++)
        {
            var cloudsNumber = data.SegmentCloudNumberRange.Random();
            var startMilestone = path.GetPointMilestone(curveIndex);
            var endMilestone = path.GetPointMilestone(curveIndex + 1);

            for (var cloudIndex = 0; cloudIndex < cloudsNumber; cloudIndex++)
            {
                var cloudPrefab = data.CloudPrefabs.PickRandom();
                var scale = data.CloudScaleRange.Random();
                var scaledCloudExtents = cloudPrefab.bounds.extents * scale;

                var lerpState = (cloudIndex + 0.5f) / cloudsNumber;
                var milestone = Mathf.Lerp(startMilestone, endMilestone, lerpState);

                var point = path.FollowPath(milestone);
                var position = point.Position;
                var forward = point.Forward.Flatened();

                var circularAngle = Random.Range(0f, 360f);
                var offsetDirection = Quaternion.AngleAxis(circularAngle, forward) * Vector3.up;

                var tmpOffset = offsetDirection * (ringRadius + scaledCloudExtents.magnitude);

                var cloud = Instantiate(cloudPrefab, position + tmpOffset, Quaternion.LookRotation(forward), cloudRoot);
                cloud.transform.localScale *= scale;

                var closestPoint = cloud.bounds.ClosestPoint(position);
                var cloudRadius = (closestPoint - cloud.bounds.center).magnitude;

                var offset = offsetDirection * (ringRadius + cloudRadius + data.CloudToPathDistanceRange.Random());
                cloud.transform.position = position + offset;
            }
        }

        // Draw guide line along path.
        guideLineDrawer.Draw(path);
    }
}
