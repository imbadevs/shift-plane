﻿using UnityEngine;


/// <summary>
/// Represents point with position and direction in space.
/// </summary>
[System.Serializable]
public class MapPoint
{
    public Vector3 Position { get => position; }
    [SerializeField] private Vector3 position;
    /// <summary> !NORMALISED! </summary>
    public Vector3 Forward { get => forward; }
    [SerializeField] private Vector3 forward;

    /// <param name="forward"> Normalises <paramref name="forward"/> before assigning. </param>
    public MapPoint(Vector3 position, Vector3 forward)
    {
        this.position = position;
        this.forward = forward.normalized;
    }

    /// <summary>
    /// Creates new <c>MapPoint</c> with updated <c>Position</c> and <c>Forward</c> (if given).
    /// </summary>
    public MapPoint NewWith(Vector3? position = null, Vector3? forward = null)
        => new MapPoint(position ?? Position, forward ?? Forward);

    /// <summary>
    /// Creates new <c>MapPoint</c> with updated <c>Position</c> coordinates (if given).
    /// </summary>
    public MapPoint NewWithPosition(float? x = null, float? y = null, float? z = null)
        => new MapPoint(Position.With(x, y, z), Forward);


    public override string ToString()
        => $"MapPoint(pos: {Position} | fwd: {Forward})";
}
