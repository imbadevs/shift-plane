﻿using UnityEngine;


namespace Curves
{
    public class CubicBezier : Bezier
    {
        public readonly Vector3 Control1;
        public readonly Vector3 Control2;

        public CubicBezier(MapPoint start, MapPoint end, Vector3 control1, Vector3 control2) : base(start, end)
        {
            Control1 = control1;
            Control2 = control2;
        }


        public static CubicBezier FromPointsAndSpeed(MapPoint start, MapPoint end, float speed)
        {
            var control1 = start.Position + start.Forward.normalized * speed;
            var control2 = end.Position - end.Forward.normalized * speed;
            return new CubicBezier(start, end, control1, control2);
        }


        public override Vector3 InterpolatePosition(float t)
        {
            return (1 - t) * (1 - t) * (1 - t) * Start.Position + 3 * (1 - t) * (1 - t) * t * Control1 + 3 * (1 - t) * t * t * Control2 + t * t * t * End.Position;
        }

        public override Vector3 InterpolateTangent(float t)
        {
            return -3 * ((1-t)*(1-t)*Start.Position - (1-t)*(1-3*t)*Control1 + t*(3*t-2)*Control2 - t*t*End.Position);
        }
    }
}