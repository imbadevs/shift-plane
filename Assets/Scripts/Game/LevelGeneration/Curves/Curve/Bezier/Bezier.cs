﻿namespace Curves
{
    public abstract class Bezier : Curve
    {
        public Bezier(MapPoint start, MapPoint end) : base(start, end)
        {
        }
    }
}