﻿using UnityEngine;

namespace Curves
{
    public interface ICurve : ILineRenderable
    {
        MapPoint Start { get; }
        MapPoint End { get; }

        MapPoint Interpolate(float t);
        Vector3 InterpolatePosition(float t);
        Vector3 InterpolateTangent(float t);
    }
}