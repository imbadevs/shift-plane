﻿using UnityEngine;

namespace Curves
{
    public abstract class Curve : ICurve
    {
        public Curve(MapPoint start, MapPoint end)
        {
            Start = start;
            End = end;
        }

        public MapPoint Start { get; }
        public MapPoint End { get; }

        public virtual MapPoint Interpolate(float t)
        {
            return new MapPoint(InterpolatePosition(t), InterpolateTangent(t));
        }

        public abstract Vector3 InterpolatePosition(float t);

        public abstract Vector3 InterpolateTangent(float t);

        public virtual int TotalVertexNumber(int sampleSegments) => sampleSegments + 1;

        public virtual float ApproximateLineWithLength(int sampleSegments, ref Vector3[] vertices, int startIndex)
        {
            var totalLength = 0f;
            vertices[startIndex] = InterpolatePosition(0f);
            for (var vertexIndex = 1; vertexIndex <= sampleSegments; vertexIndex++)
            {
                var currentVertexIndex = startIndex + vertexIndex;
                vertices[currentVertexIndex] = InterpolatePosition((float)vertexIndex / sampleSegments);
                var segment = vertices[currentVertexIndex] - vertices[currentVertexIndex - 1];
                totalLength += segment.magnitude;
            }
            return totalLength;
        }

        public virtual float ApproximateLength(int sampleSegments)
        {
            var totalLength = 0f;
            var prevVertex = InterpolatePosition(0f);
            for (var vertexIndex = 1f; vertexIndex <= sampleSegments; vertexIndex++)
            {
                var vertex = InterpolatePosition(vertexIndex / sampleSegments);
                var segment = vertex - prevVertex;
                totalLength += segment.magnitude;
                prevVertex = vertex;
            }
            return totalLength;
        }

        public virtual void ApproximateLine(int sampleSegments, ref Vector3[] vertices, int startIndex)
        {
            for (var vertexIndex = 0; vertexIndex <= sampleSegments; vertexIndex++)
            {
                vertices[startIndex + vertexIndex] = InterpolatePosition((float)vertexIndex / sampleSegments);
            }
        }
    }
}