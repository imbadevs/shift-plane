﻿using UnityEngine;

namespace Curves
{
    public class Spline : ICurve
    {
        private readonly ICurve[] curves;

        public int CurveNumber { get => curves.Length; }
        public MapPoint Start { get; }
        public MapPoint End { get; }


        public Spline(ICurve[] curves)
        {
            this.curves = curves;
            Start = curves[0].Start;
            End = curves.Last().End;
        }

        //public static Spline FromPointsWithSpeed(MapPoint[] points, float speed)
        //{
        //    var curves = new Curve[points.Length - 1];
        //    for (var index = 0; index < points.Length - 1; index++)
        //    {
        //        curves[index] = CurveFactory.CreateCurve(points[index], points[index + 1], speed);
        //    }
        //    return new Spline(curves);
        //}

        public int TotalVertexNumber(int sampleSegmentsPerCurve) => sampleSegmentsPerCurve * curves.Length + 1;

        public float ApproximateLength(int sampleSegmentsPerCurve)
        {
            var totalLength = 0f;
            foreach (var curve in curves)
            {
                totalLength += curve.ApproximateLength(sampleSegmentsPerCurve);
            }
            return totalLength;
        }
        public void ApproximateLine(int sampleSegmentsPerCurve, ref Vector3[] vertices, int startIndex)
        {
            for (var curve = 0; curve < curves.Length; curve++)
            {
                curves[curve].ApproximateLine(sampleSegmentsPerCurve, ref vertices, curve * sampleSegmentsPerCurve);
            }
        }
        public float ApproximateLineWithLength(int sampleSegmentsPerCurve, ref Vector3[] vertices, int startIndex)
        {
            var totalLength = 0f;
            for (var curve = 0; curve < curves.Length; curve++)
            {
                totalLength += curves[curve].ApproximateLineWithLength(sampleSegmentsPerCurve, ref vertices, curve * sampleSegmentsPerCurve);
            }
            return totalLength;
        }


        public Vector3 InterpolatePosition(int curveIndex, float t)
        {
            return curves[curveIndex].InterpolatePosition(t);
        }
        public Vector3 InterpolateTangent(int curveIndex, float t)
        {
            return curves[curveIndex].InterpolateTangent(t);
        }
        public MapPoint Interpolate(int curveIndex, float t)
        {
            return curves[curveIndex].Interpolate(t);
        }


        public Vector3 InterpolatePosition(float t)
        {
            (var curveIndex, var curveState) = SplineStateToCurveState(t);
            return InterpolatePosition(curveIndex, curveState);
        }
        public Vector3 InterpolateTangent(float t)
        {
            (var curveIndex, var curveState) = SplineStateToCurveState(t);
            return InterpolateTangent(curveIndex, curveState);
        }
        public MapPoint Interpolate(float t)
        {
            (var curveIndex, var curveState) = SplineStateToCurveState(t);
            return Interpolate(curveIndex, curveState);
        }

        private (int, float) SplineStateToCurveState(float t)
        {
            var rawCurve = t * CurveNumber;
            var curveIndex = Mathf.FloorToInt(rawCurve);
            var curveState = rawCurve - curveIndex;
            if (curveIndex == CurveNumber)
            {
                --curveIndex;
                curveState = 1f;
            }
            return (curveIndex, curveState);
        }
    }
}