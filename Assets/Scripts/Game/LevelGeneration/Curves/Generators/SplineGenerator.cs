﻿using System;

namespace Curves
{
    public class SplineGenerator
    {
        private readonly CurveGenerator generator;

        public SplineGenerator(CurveGenerator generator)
        {
            this.generator = generator;
        }

        public Spline NewSpline(MapPoint[] points)
        {
            var curvesNumber = points.Length - 1;
            var curves = new ICurve[curvesNumber];
            for (var curve = 0; curve < curvesNumber; curve++)
            {
                curves[curve] = generator.NewCurve(points[curve], points[curve + 1]);
            }
            return new Spline(curves);
        }
    }


    public static class SplineGenerators
    {
        public static SplineGenerator CubicBezierWithSpeed(float speed)
        {
            return new SplineGenerator(CurveGenerators.CubicBezierWithSpeed(speed));
        }
    }
}