﻿using System;

namespace Curves
{
    public class CurveGenerator
    {
        private readonly Func<MapPoint, MapPoint, ICurve> generator;

        public CurveGenerator(Func<MapPoint, MapPoint, ICurve> generator)
        {
            this.generator = generator;
        }

        public ICurve NewCurve(MapPoint start, MapPoint end) => generator(start, end);
    }


    public static class CurveGenerators
    {
        public static CurveGenerator CubicBezierWithSpeed(float speed)
        {
            return new CurveGenerator((start, end) =>
            {
                var control1 = start.Position + start.Forward.normalized * speed;
                var control2 = end.Position - end.Forward.normalized * speed;
                return new CubicBezier(start, end, control1, control2);
            });
        }
    }
}