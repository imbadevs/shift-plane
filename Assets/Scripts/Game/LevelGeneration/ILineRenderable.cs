﻿using UnityEngine;


public interface ILineRenderable
{
    int TotalVertexNumber(int sampleSegments);
    float ApproximateLineWithLength(int sampleSegments, ref Vector3[] vertices, int startIndex);
    float ApproximateLength(int sampleSegments);
    void ApproximateLine(int sampleSegments, ref Vector3[] vertices, int startIndex);
}


public static class LineRenderableExtension
{
    public static Vector3[] ApproximateLine(this ILineRenderable path, int sampleSegments)
    {
        var vertices = new Vector3[path.TotalVertexNumber(sampleSegments)];
        path.ApproximateLine(sampleSegments, ref vertices, 0);
        return vertices;
    }

    public static ApproximatedLineWithLength ApproximateLineWithLength(this ILineRenderable path, int sampleSegments)
    {
        var vertices = new Vector3[path.TotalVertexNumber(sampleSegments)];
        var length = path.ApproximateLineWithLength(sampleSegments, ref vertices, 0);
        return new ApproximatedLineWithLength(vertices, length);
    }
}


public struct ApproximatedLineWithLength
{
    public readonly Vector3[] Vertices;
    public readonly float Length;

    public ApproximatedLineWithLength(Vector3[] vertices, float length)
    {
        Vertices = vertices;
        Length = length;
    }
}