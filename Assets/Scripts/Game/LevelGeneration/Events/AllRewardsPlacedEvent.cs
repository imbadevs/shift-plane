﻿namespace Events.LevelGeneration
{
    class AllRewardsPlacedEvent
    {
        public readonly int RewardsNumber;

        public AllRewardsPlacedEvent(int rewardsNumber)
        {
            RewardsNumber = rewardsNumber;
        }
    }
}
