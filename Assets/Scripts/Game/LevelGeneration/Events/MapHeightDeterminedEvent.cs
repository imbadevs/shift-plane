﻿namespace Events.LevelGeneration
{
    class MapHeightDeterminedEvent
    {
        public readonly float MaxY;
        public readonly float MinY;

        public MapHeightDeterminedEvent(float minY, float maxY)
        {
            MaxY = maxY;
            MinY = minY;
        }
    }
}
