﻿namespace Events.LevelGeneration
{
    class PathCreatedEvent
    {
        public readonly Path Path;

        public PathCreatedEvent(Path path)
        {
            Path = path;
        }
    }
}
