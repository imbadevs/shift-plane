﻿namespace Events.LevelGeneration
{
    class PlanePathCreatedEvent
    {
        public readonly Path Path;

        public PlanePathCreatedEvent(Path path)
        {
            Path = path;
        }
    }
}

