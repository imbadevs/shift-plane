﻿namespace Events.LevelGeneration
{
    class LevelGenerationConfigsCreatedEvent
    {
        public readonly PathPointsGenerationData PathPointsGenerationData;
        public readonly PropsPlacementData PropsPlacementData;

        public LevelGenerationConfigsCreatedEvent(PathPointsGenerationData pathPointsGenerationData, PropsPlacementData propsPlacementData)
        {
            PathPointsGenerationData = pathPointsGenerationData;
            PropsPlacementData = propsPlacementData;
        }
    }
}

