﻿using UnityEngine;


/// <summary>
/// Uses <c>LineRenderer</c> to draw given <c>ILineRenderable</c>.
/// </summary>
class GuideLineDrawer : MonoBehaviour
{
    [SerializeField] private LineRenderer line;
    [SerializeField] private int segmentsPerCurve = 25;


    /// <summary>
    /// Approximately draws <paramref name="renderable"/>.
    /// </summary>
    /// <param name="renderable"> Line to draw. </param>
    public void Draw(ILineRenderable renderable)
    {
        var vertices = renderable.ApproximateLine(segmentsPerCurve);
        line.positionCount = vertices.Length;
        line.SetPositions(vertices);
    }
}