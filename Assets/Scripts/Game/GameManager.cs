﻿using GlobalEventSystem;

using UnityEngine;
using UnityEngine.SceneManagement;

class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private int rewards;

    [SerializeField] private UIController uiController;

    [SerializeField] private float timeScale = 1f;


    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        EventDispatcher.Instance.Subscribe<Events.Game.RewardEvent>(Reward);
        EventDispatcher.Instance.Subscribe<Events.Game.PlayerKilledEvent>(OnPlayerKilled);
    }



    private void Update()
    {
#if UNITY_EDITOR
        Time.timeScale = timeScale;
#endif
    }

    public void GoToMenu()
    {
        Debug.Log("Going to menu");
        EventDispatcher.Instance.UnsubscribeAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Reward(Events.Game.RewardEvent _)
    {
        rewards++;
        EventDispatcher.Instance.Dispatch(new Events.Game.ScoreChangeEvent(rewards));
    }

    public void Finish()
    {
        EventDispatcher.Instance.Dispatch(new Events.Game.LevelFinishedEvent(rewards));
        uiController.FinishLevel();
    }

    public void GameOver()
    {
        uiController.GameOver();
    }

    private void OnPlayerKilled(Events.Game.PlayerKilledEvent _)
    {
        Debug.Log("Player killed");
        GameOver();
    }

    public void StartGame()
    {
        uiController.StartGame();
        EventDispatcher.Instance.Dispatch(new Events.Game.PreStartPlayerAnimationEvent());
    }
}
