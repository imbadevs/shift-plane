﻿using System.Collections;

using UnityEngine;

public class GuidelineWidthController : MonoBehaviour
{
    [SerializeField] private LineRenderer guideline;
    [SerializeField] private float timeMultiplier = 0.33f;
    [SerializeField] private float hidingDuration = 0.3f;

    private AnimationCurve widthCurve;

    private Keyframe begin;
    private Keyframe mark;
    private Keyframe end;

    private Coroutine moveMarkCoroutine;

    private void Awake()
    {
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.Game.FinishReachedEvent>(Hide);
    }

    private void Start()
    {
        begin = new Keyframe(0.05f, 1f);
        begin.inTangent = 5f;
        mark = new Keyframe(0.1f, 1f);
        end = new Keyframe(0.15f, 1f);
        end.outTangent = -5f;
        widthCurve = new AnimationCurve();
        var f = widthCurve.AddKey(new Keyframe(0f, .5f));
        widthCurve.AddKey(begin);
        var s = widthCurve.AddKey(mark);
        widthCurve.AddKey(end);
        var t = widthCurve.AddKey(new Keyframe(1f, .5f));
        guideline.widthCurve = widthCurve;

        moveMarkCoroutine = StartCoroutine(MoveMark());
    }

    private void Hide(Events.Game.FinishReachedEvent _)
    {
        StopCoroutine(moveMarkCoroutine);
        StartCoroutine(Hiding());
    }

    private IEnumerator MoveMark()
    {
        while (true)
        {
            var time = Mathf.Clamp(Time.time * timeMultiplier % 1, 0.01f, 0.99f);
            begin.time = Mathf.Clamp(time - 0.01f, 0.005f, 0.985f);
            mark.time = time;
            end.time = Mathf.Clamp(time + 0.01f, 0.015f, 0.995f);
            widthCurve.MoveKey(1, begin);
            widthCurve.MoveKey(2, mark);
            widthCurve.MoveKey(3, end);
            guideline.widthCurve = widthCurve;
            yield return null;
        }
    }

    private IEnumerator Hiding()
    {
        widthCurve.keys = new Keyframe[1];
        var kf = new Keyframe(0, 0.5f);
        for (var timer = hidingDuration; timer > 0; timer -= Time.deltaTime)
        {
            kf.value = timer / hidingDuration / 2;
            widthCurve.MoveKey(0, kf);
            guideline.widthCurve = widthCurve;
            yield return null;
        }

        gameObject.SetActive(false);
    }

    //private void print(Keyframe[] kfs)
    //{
    //    var buffer = new System.Text.StringBuilder();
    //    for (var i = 0; i < kfs.Length; ++i)
    //    {
    //        buffer.Append($" | {i}: {kfs[i].time} -> {kfs[i].value}");
    //    }
    //    Debug.Log(buffer.ToString());
    //}
}
