﻿using System.Collections;

using GlobalEventSystem;

using UnityEngine;

public class GuidelineHighlighter : MonoBehaviour
{
    [SerializeField] private LineRenderer guideline;
    [Header("Settings")]
    [SerializeField] private float highlightOffset;
    [SerializeField] private float highlightLength;
    [SerializeField] private Color highlightColor;
    [Header("Hiding")]
    [SerializeField] private float hidingDuration = 0.3f;

    private float mapLength;
    private Gradient gradient;
    private GradientAlphaKey[] alphaKeys;


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.LevelGeneration.PathCreatedEvent>(Initiate);
        EventDispatcher.Instance.Subscribe<Events.Game.PlayerMovedEvent>(UpdateHighlight);
        EventDispatcher.Instance.Subscribe<Events.Game.FinishReachedEvent>(StartHiding);
    }

    private void Initiate(Events.LevelGeneration.PathCreatedEvent data)
    {
        mapLength = data.Path.Length;

        var colorKey = new GradientColorKey[] { new GradientColorKey(highlightColor, 0) };

        alphaKeys = new GradientAlphaKey[] { new GradientAlphaKey(0, 0), new GradientAlphaKey(1, 0), new GradientAlphaKey(0, 0) };
        UpdateAlphaKeys(0 + highlightOffset);

        gradient = new Gradient();
        gradient.SetKeys(colorKey, alphaKeys);
        guideline.colorGradient = gradient;
    }


    private void UpdateAlphaKeys(float position)
    {
        alphaKeys[0].time = Mathf.Clamp(position / mapLength, 0f, 0.999f);
        alphaKeys[1].time = Mathf.Clamp((position + highlightLength / 2) / mapLength, 0.0005f, 0.9995f);
        alphaKeys[2].time = Mathf.Clamp((position + highlightLength) / mapLength, 0.001f, 1f);
    }

    private void UpdateHighlight(Events.Game.PlayerMovedEvent data)
    {
        UpdateAlphaKeys(data.FractionalPosition * mapLength + highlightOffset);
        gradient.alphaKeys = alphaKeys;
        guideline.colorGradient = gradient;
    }


    private void StartHiding(Events.Game.FinishReachedEvent _)
    {
        StartCoroutine(Hide());
    }

    private IEnumerator Hide()
    {
        var initialAlpha = alphaKeys[1].alpha;
        for (var timer = 0f; timer <= hidingDuration; timer += Time.deltaTime)
        {
            alphaKeys[1].alpha = Mathf.Lerp(initialAlpha, 0, timer / hidingDuration);
            gradient.alphaKeys = alphaKeys;
            guideline.colorGradient = gradient;
            yield return null;
        }
    }
}
