﻿using GlobalEventSystem;

using UnityEngine;

class CollectiblePathFollower : MonoBehaviour
{
    [SerializeField] private float verticalSpeed = 5f;
    [SerializeField] private FloatRange verticalPositionDelta = new FloatRange(-2, 2);
    [SerializeField] private float speed = 15f;
    [SerializeField] private float minDistanceToPlayer = 80f;
    [Range(0f, 1f)]
    [SerializeField] private float fractionalStartingPosition;

    private Path path;
    private float absolutePosition;

    private SubscriberID<GlobalEvent<Events.Game.PlayerMovedEvent>> playerMovedSubscription;
    private bool isMoving = false;
    private float moveStartTime;


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.LevelGeneration.PathCreatedEvent>(Initialise);
        playerMovedSubscription = EventDispatcher.Instance.Subscribe<Events.Game.PlayerMovedEvent>(CheckPlayerProximity);
    }

    private void Initialise(Events.LevelGeneration.PathCreatedEvent data)
    {
        path = data.Path;
        absolutePosition = fractionalStartingPosition * path.Length;
    }

    private void CheckPlayerProximity(Events.Game.PlayerMovedEvent player)
    {
        if (absolutePosition - player.AbsolutePosition < minDistanceToPlayer)
        {
            isMoving = true;
            moveStartTime = Time.time;
            EventDispatcher.Instance.Unsubscribe(playerMovedSubscription);
        }
    }


    private void Update()
    {
        if (isMoving)
        {
            absolutePosition += speed * Time.deltaTime;
            var point = path.FollowPath(absolutePosition);
            var yDelta = verticalPositionDelta.PingPong((Time.time - moveStartTime) * verticalSpeed);
            transform.position = point.Position.With(y: point.Position.y + yDelta);
            transform.forward = point.Forward.Flatened();
        }
    }
}
