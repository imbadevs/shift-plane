﻿using UnityEngine;

public class Collectible : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            Collect();
            DestroySelf();
        }
    }

    private void Collect()
    {
        Debug.Log("Award Collected!");
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }
}
