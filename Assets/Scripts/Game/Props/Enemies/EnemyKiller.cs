﻿using GlobalEventSystem;

using UnityEngine;


public class EnemyKiller : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            EventDispatcher.Instance.Dispatch(new Events.Game.PlayerKilledEvent());
        }
    }
}
