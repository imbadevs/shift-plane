﻿using System.Collections;

using UnityEngine;

public class FinishEnemyHider : MonoBehaviour
{
    [SerializeField] private float duration = 0.3f;

    private void Awake()
    {
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.Game.FinishReachedEvent>((_) => StartCoroutine(Hide()));
    }

    private IEnumerator Hide()
    {
        var state = 1f;
        for (var timer = duration; timer >= 0; timer -= Time.deltaTime)
        {
            state = timer / duration;
            transform.localScale = new Vector3(state, state, state);
            yield return null;
        }

        Destroy(gameObject);
    }
}
