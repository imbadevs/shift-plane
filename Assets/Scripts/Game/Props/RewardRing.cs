﻿using GlobalEventSystem;

using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RewardRing : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private Fireworks fireworksPrefab;
    [SerializeField] private Cinemachine.CinemachineImpulseSource impulseSource;


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerRewardHitBox>() != null)
        {
            Reward();
        }
    }

    private void Reward()
    {
        EventDispatcher.Instance.Dispatch(new Events.Game.RewardEvent());
        BlowUp();
    }

    private void BlowUp()
    {
        animator.SetTrigger("Collected");
    }

    // Will be called from animation event.
    private void DestroySelfAfterAnimation()
    {
        var fireworks = Instantiate(fireworksPrefab, transform.position, Quaternion.identity);
        fireworks.Play();
        impulseSource.GenerateImpulse();
        Destroy(transform.parent.gameObject);
    }
}
