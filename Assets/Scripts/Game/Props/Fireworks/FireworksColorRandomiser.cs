﻿using UnityEngine;

class FireworksColorRandomiser : MonoBehaviour
{
    [SerializeField] private ParticleSystem fireworks;

    private void Awake()
    {
        var main = fireworks.main;
        var startColor = main.startColor;
        var colorKeys = new GradientColorKey[2];
        var firstValue = Random.value;
        var firstColor = startColor.gradient.Evaluate(firstValue);
        var secondValue = 1f - firstValue;
        var secondColor = startColor.gradient.Evaluate(secondValue);
        colorKeys[0] = new GradientColorKey(firstColor, 0.5f);
        colorKeys[1] = new GradientColorKey(secondColor, 1f);
        var newGradient = new Gradient();
        newGradient.mode = GradientMode.Fixed;
        newGradient.colorKeys = colorKeys;
        startColor = newGradient;
        startColor.mode = ParticleSystemGradientMode.RandomColor;
        main.startColor = startColor;
    }
}
