﻿using UnityEngine;

class Fireworks : MonoBehaviour
{
    [Header("Required Components")]
    [SerializeField] private ParticleSystem particles;
    [SerializeField] private AudioSource audioSource;
    [Header("Settings")]
    [SerializeField] private bool playOnAwake = false;
    [Header("Audio")]
    [SerializeField] private FloatRange pitchDeltaRange;


    private void Awake()
    {
        if (playOnAwake)
        {
            Play();
        }
    }

    public void Play(float audioDelay = 0f)
    {
        particles.Play();
        PlaySFX(audioDelay);
    }

    private void PlaySFX(float delay)
    {
        audioSource.pitch += pitchDeltaRange.Random();
        audioSource.PlayDelayed(delay);
    }
}
