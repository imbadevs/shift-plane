﻿using UnityEngine;

public class FinishApplause : MonoBehaviour
{
    [SerializeField] private AudioSource applause;

    private void Awake()
    {
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.Game.FinishReachedEvent>(Applause);
    }

    private void Applause(Events.Game.FinishReachedEvent _)
    {
        applause.Play();
    }
}
