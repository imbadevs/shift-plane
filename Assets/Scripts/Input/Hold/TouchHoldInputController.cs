﻿class TouchHoldInputController : IHoldInputController
{
    public bool Input => UnityEngine.Input.touchCount > 0;
}
