﻿interface ISlideInputController
{
    float GetInput { get; }
}
