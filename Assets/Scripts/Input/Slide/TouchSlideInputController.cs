﻿using UnityEngine;

class TouchSlideInputController : ISlideInputController
{
    private const float multiplier = 25f;

    private bool isTrackingFinger = false;
    private int trackedFingerId;


    public float GetInput => ReadInput() / Screen.height * multiplier;


    private float ReadInput()
    {
        if (Input.touchCount > 0)
        {
            if (isTrackingFinger)
            {
                if (Input.touches.FindExplicit(t => t.fingerId == trackedFingerId, out var touch))
                {
                    return touch.deltaPosition.y;
                }

            }
            trackedFingerId = Input.touches[0].fingerId;
            return Input.touches[0].deltaPosition.y;
        }
        isTrackingFinger = false;

        return 0f;
    }
}
