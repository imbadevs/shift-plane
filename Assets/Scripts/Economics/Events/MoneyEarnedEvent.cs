﻿namespace Events.Economics
{
    class MoneyEarnedEvent
    {
        public readonly int MoneyEarned;

        public MoneyEarnedEvent(int moneyEarned)
        {
            MoneyEarned = moneyEarned;
        }
    }
}
