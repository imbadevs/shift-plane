﻿using UnityEngine;

public class Dev_EditorSlideInputController : ISlideInputController
{
    private float multiplier = .2f;

    public float GetInput => ReadInput() * multiplier;

    private float ReadInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            return 1f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            return -1f;
        }
        return 0f;
    }
}
