﻿using System;
using System.Collections;
using System.Collections.Generic;
using GlobalEventSystem;
using UnityEngine;
using UnityEngine.UI;

public class Dev_LevelWinner : MonoBehaviour
{
    [SerializeField] private Button button;

    private void Awake()
    {
        button.onClick.AddListener(Win);
    }

    private void Win()
    {
        EventDispatcher.Instance.Dispatch(new Events.Game.FinishReachedEvent());
    }
}
