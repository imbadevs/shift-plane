﻿using UnityEngine;

public class Dev_PlayerPrefsCleaner : MonoBehaviour
{
    public void CleanPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameManager.Instance.GoToMenu();
    }
}
