﻿class Dev_EditorHoldInputController : IHoldInputController
{
    public bool Input => UnityEngine.Input.GetKey(UnityEngine.KeyCode.Space) || UnityEngine.Input.GetKey(UnityEngine.KeyCode.W);
}
