﻿using UnityEngine;

[System.Serializable]
abstract class MenuController : MonoBehaviour
{
    [Header("Animation")]
    [SerializeField] protected Animator animator;

    public void Show()
    {
        animator.SetBool("Show", true);
    }
    public void Hide()
    {
        animator.SetBool("Show", false);
    }
}
