﻿using TMPro;

using UnityEngine;
using UnityEngine.UI;

class MainMenuController : MenuController
{
    [Header("Buttons")]
    [SerializeField] private Button settings;
    [SerializeField] private Button achievments;
    [SerializeField] private Button start;

    [Header("Texts")]
    [SerializeField] private TMP_Text moneyText;

    private void Awake()
    {
        settings.onClick.AddListener(OpenSettings);
        achievments.onClick.AddListener(OpenAchievments);
        start.onClick.AddListener(StartGame);

        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.Storage.MoneyLoadedEvent>(eventData => UpdateMoneyText(eventData.Money));
    }

    private void UpdateMoneyText(int newMoney)
    {
        var str = newMoney.ToString();
        var index = str.Length;
        while ((index -= 3) > 0)
        {
            str = str.Insert(index, " ");
        }
        moneyText.text = $"{str} <sprite color=#ffff00 name=\"ring\">";
    } 

    private void StartGame()
    {
        GameManager.Instance.StartGame();
    }

    private void OpenAchievments()
    {
        Debug.Log("Look at dem great DEEDS!");
    }

    private void OpenSettings()
    {
        Debug.Log("So much settings to tweak maaan");
    }
}
