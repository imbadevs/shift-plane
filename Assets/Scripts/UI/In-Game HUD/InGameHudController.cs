﻿using GlobalEventSystem;

using TMPro;

using UnityEngine;

class InGameHudController : MenuController
{
    [Header("Texts")]
    [SerializeField] private TMP_Text scoreText;

    private int maxScore;
    private int currentScore;


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.LevelGeneration.AllRewardsPlacedEvent>(UpdateMaxScore);
        EventDispatcher.Instance.Subscribe<Events.Game.ScoreChangeEvent>(UpdateCurrentScore);
    }

    public void UpdateMaxScore(Events.LevelGeneration.AllRewardsPlacedEvent eventData)
    {
        maxScore = eventData.RewardsNumber;
        UpdateScoreText();
    }

    private void UpdateCurrentScore(Events.Game.ScoreChangeEvent eventData)
    {
        currentScore = eventData.NewScore;
        UpdateScoreText();
    }


    private void UpdateScoreText()
    {
        scoreText.text = $"<sprite color=#ffff00 name=\"ring\"> {currentScore} | {maxScore} <sprite color=#ffff00 name=\"ring\">";
    }
}
