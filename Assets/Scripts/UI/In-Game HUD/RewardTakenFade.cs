﻿using GlobalEventSystem;

using UnityEngine;

public class RewardTakenFade : MonoBehaviour
{
    [SerializeField] private Animator animator;

    private const string FadeTrigger = "Fade";


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.Game.RewardEvent>(PlayFadeAnimation);
    }


    private void PlayFadeAnimation(Events.Game.RewardEvent _)
    {
        animator.SetTrigger(FadeTrigger);
    }
}
