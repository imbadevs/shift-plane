﻿using UnityEngine;
using UnityEngine.UI;

class GameOverPopUpController : MenuController
{
    [Header("Buttons")]
    [SerializeField] private Button adContinueButton;
    [SerializeField] private Button noThanksButton;

    [Header("Sliders")]
    [SerializeField] private Slider adContinueSlider;


    private void Start()
    {
        noThanksButton.onClick.AddListener(GameManager.Instance.GoToMenu);
    }
}
