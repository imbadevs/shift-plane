﻿using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private MenuController mainMenu;
    [SerializeField] private MenuController inGameHud;
    [SerializeField] private MenuController gameOver;
    [SerializeField] private MenuController levelFinish;


    public void StartGame()
    {
        mainMenu.Hide();
        inGameHud.Show();
    }


    public void GameOver()
    {
        inGameHud.Hide();
        gameOver.Show();
    }


    public void FinishLevel()
    {
        inGameHud.Hide();
        levelFinish.Show();
    }


    public void GoToMenu()
    {
        inGameHud.Hide();
        gameOver.Hide();
        levelFinish.Hide();
        mainMenu.Show();
    }
}
