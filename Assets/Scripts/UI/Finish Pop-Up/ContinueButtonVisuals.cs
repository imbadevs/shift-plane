﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Utils;


public class ContinueButtonVisuals : MonoBehaviour
{
    [SerializeField] private Button button;

    private Graphic buttonGraphic;


    private void Awake()
    {
        button.interactable = true;
        button.enabled = false;
        buttonGraphic = button.targetGraphic;
        var color = buttonGraphic.color;
        color.a = 0;
        buttonGraphic.color = color;
    }


    public void StartVisuals(float delay, float duration)
    {
        StartCoroutine(FadeIn(delay, duration));
    }

    private IEnumerator FadeIn(float delay, float duration)
    {
        var color = buttonGraphic.color;
        yield return Timer
            .WithStartAndDuration(Time.time + delay, duration)
            .Every(null)
            .Repeat(state =>
            {
                color.a = state;
                buttonGraphic.color = color;
            }).CreateCoroutine();
        button.enabled = true;
    }
}
