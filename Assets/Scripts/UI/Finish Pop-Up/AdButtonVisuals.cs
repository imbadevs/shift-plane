﻿using System.Collections;
using UnityEngine;
using Utils;


public class AdButtonVisuals : MonoBehaviour
{
    [SerializeField] private RectTransform ad;
    [SerializeField] private TimerVisuals timer;
    [SerializeField] private ContinueButtonVisuals continueVisuals;

    private void Awake()
    {
        ad.localScale = new Vector3(0, 0, 1);
    }

    public void StartVisuals(float fadeInDuration, float timerDuration, float continueDelay, float continueFadeInDuration)
    {
        StartCoroutine(Visuals(fadeInDuration, timerDuration, continueDelay, continueFadeInDuration));
    }

    private IEnumerator Visuals(float fadeInTime, float timerDuration, float continueDelay, float continueFadeInDuration)
    {
        yield return FadeIn(fadeInTime);
        timer.StartVisuals(timerDuration);
        continueVisuals.StartVisuals(continueDelay, continueFadeInDuration);
    }

    private IEnumerator FadeIn(float fadeInTime)
    {
        yield return Timer
            .WithStartAndDuration(Time.time, fadeInTime)
            .Every(null)
            .Repeat(state =>
            {
                var scaleFactor = Easings.EaseIn(3, state);
                ad.localScale = new Vector3(scaleFactor, scaleFactor, 1f);
            })
            .CreateCoroutine();
    }
}
