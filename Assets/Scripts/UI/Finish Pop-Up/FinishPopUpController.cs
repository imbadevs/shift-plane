﻿using GlobalEventSystem;

using UnityEngine;
using UnityEngine.UI;


class FinishPopUpController : MenuController
{
    [Header("Buttons")]
    [SerializeField] private Button adDoubleScoreButton;
    [SerializeField] private Button noThanksButton;


    private int finalScore;


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.Game.LevelFinishedEvent>(data => finalScore = data.FinalScore);
        EventDispatcher.Instance.Subscribe<Events.UI.AdTimerRanOut>(_ => FinalProcessing(false));
        noThanksButton.onClick.AddListener(() => FinalProcessing(false));
        adDoubleScoreButton.onClick.AddListener(ShowAd);
    }

    private void ShowAd()
    {
        Debug.Log("Gonna show ad..");
    }


    private void FinalProcessing(bool isAwarded)
    {
        EventDispatcher.Instance.Dispatch(new Events.Economics.MoneyEarnedEvent(finalScore * (isAwarded ? 2 : 1)));
        GameManager.Instance.GoToMenu();
    }


    // Called from show animation.
    private void ShowAnimationFinished()
    {
        EventDispatcher.Instance.Dispatch(new Events.UI.FinishPopUpShownEvent());
    }
}
