﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class TimerVisuals : MonoBehaviour
{
    [SerializeField] private Slider timerSlider;
    [SerializeField] private RectTransform planePivot;
    [SerializeField] private RectTransform plane;

    private const float radialAngle = 360f;
    private const float hidingAngle = 45f;
    private const float hidingDuration = 1.5f;
    private Vector3 axis = Vector3.forward;

    private float speed;


    public void StartVisuals(float timerDuration)
    {
        var radius = plane.localPosition.magnitude;
        var pathLength = 2 * Mathf.PI * radius;
        speed = pathLength / timerDuration;
        StartCoroutine(UpdateVisuals(speed, timerDuration));
    }


    private IEnumerator UpdateVisuals(float speed, float duration)
    {
        yield return Timer
            .WithStartAndDuration(Time.time, duration)
            .Every(null)
            .Repeat(state =>
            {
                var pivotAngle = -radialAngle * state;
                planePivot.localRotation = Quaternion.AngleAxis(pivotAngle, axis);
                timerSlider.normalizedValue = 1 - state;
            }).CreateCoroutine();

        var up = plane.up;
        yield return Timer
            .WithStartAndDuration(Time.time, hidingDuration)
            .Every(null)
            .Repeat(state =>
            {
                var angle = hidingAngle * state;
                var rotation = Quaternion.AngleAxis(angle, axis);
                plane.up = rotation * up;
                plane.position += -plane.up * speed * Time.deltaTime;
            }).CreateCoroutine();
    }
}
