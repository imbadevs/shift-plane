﻿using System.Collections;
using UnityEngine;
using TMPro;
using GlobalEventSystem;
using Utils;

public class FinalScore : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI finalScoreText;
    [SerializeField] private Animator[] stars;

    [SerializeField] private float scoreCountDuration = 1f;

    private int maxScore;
    private int score;

    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.LevelGeneration.AllRewardsPlacedEvent>(data => { maxScore = data.RewardsNumber; UpdateText(0); });
        EventDispatcher.Instance.Subscribe<Events.Game.LevelFinishedEvent>(data => score = data.FinalScore);
        EventDispatcher.Instance.Subscribe<Events.UI.FinishPopUpShownEvent>(StartCounting);
    }


    private void StartCounting(Events.UI.FinishPopUpShownEvent _)
    {
        StartCoroutine(Counting());
    }

    private IEnumerator Counting()
    {
        var tmpScore = 0;
        var stars = 0;
        yield return Timer
            .WithStartAndDuration(Time.time, scoreCountDuration)
            .Every(null)
            .Repeat(state =>
            {
                state = Easings.EaseInOut(2, state);
                tmpScore = Mathf.RoundToInt(Mathf.Lerp(0, score, state));
                UpdateText(tmpScore);
                var tmpStars = CountStars(tmpScore);
                if (stars != tmpStars)
                {
                    stars = tmpStars;
                    FillStar(stars - 1);
                }
            }).CreateCoroutine();
        EventDispatcher.Instance.Dispatch(new Events.UI.ScoreCountingFinished());
    }

    private void UpdateText(int? tmpScore = null)
    {
        finalScoreText.text = $"<sprite name=\"ring\" color=#FFFF00> <color=#ffff00>{tmpScore ?? score}</color> | {maxScore} <sprite name=\"ring\" color=#FFFF00>";
    }

    private int CountStars(int score)
    {
        return Mathf.FloorToInt(3f * score / maxScore);
    }

    private void FillStar(int index)
    {
        stars[index].SetTrigger("pop");
    }
}
