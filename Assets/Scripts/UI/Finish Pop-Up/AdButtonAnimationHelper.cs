﻿using GlobalEventSystem;
using UnityEngine;


public class AdButtonAnimationHelper : MonoBehaviour
{
    public void AdButtonFadedIn()
    {
        EventDispatcher.Instance.Dispatch(new Events.UI.AdButtonShown());
    }

    public void AdTimerRanOut()
    {
        EventDispatcher.Instance.Dispatch(new Events.UI.AdTimerRanOut());
    }
}
