﻿using System;
using System.Collections;
using Events.Game;
using UnityEngine;
using UnityEngine.UI;

public class FinishAdController : MonoBehaviour
{
    [SerializeField] private float adTimerDuration = 5f;
    [SerializeField] private float continueDelay = 1f;
    [SerializeField] private float continueFadeInDuration = 0.5f;

    [SerializeField] private Animator adButtonAnimator;
    [SerializeField] private ContinueButtonVisuals continueVisuals;
    [SerializeField] private TMPro.TextMeshProUGUI x2Text;


    private const float defaultTimerDuration = 5f;


    private void Awake()
    {
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.Game.LevelFinishedEvent>(SaveScore);
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.UI.FinishPopUpShownEvent>(StartAdTimer);
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.UI.AdButtonShown>(ShowContinueButton);
        PrepareAnimation();
    }

    private void SaveScore(LevelFinishedEvent data)
    {
        x2Text.text = (data.FinalScore * 2).ToString();
    }

    private void PrepareAnimation()
    {
        adButtonAnimator.speed = defaultTimerDuration / adTimerDuration;
    }

    private void StartAdTimer(Events.UI.FinishPopUpShownEvent _)
    {
        adButtonAnimator.enabled = true;
    }

    public void ShowContinueButton(Events.UI.AdButtonShown _)
    {
        continueVisuals.StartVisuals(continueDelay, continueFadeInDuration);
    }
}
