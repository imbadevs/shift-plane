﻿namespace Events.Storage
{
    class LevelLoadedEvent
    {
        public readonly int LoadedLevel;

        public LevelLoadedEvent(int loadedLevel)
        {
            LoadedLevel = loadedLevel;
        }
    }
}
