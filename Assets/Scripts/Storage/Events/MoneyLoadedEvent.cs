﻿namespace Events.Storage
{
    class MoneyLoadedEvent
    {
        public readonly int Money;

        public MoneyLoadedEvent(int money)
        {
            Money = money;
        }
    }
}
