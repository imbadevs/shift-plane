﻿using Events.Economics;
using Events.Game;

using GlobalEventSystem;

using UnityEngine;

public class StorageManager : MonoBehaviour
{
    private const string Level = "LEVEL";
    private const string Money = "MONEY";

    private int currentLevel;
    private int currentMoney;

    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.Economics.MoneyEarnedEvent>(SaveMoney);
        EventDispatcher.Instance.Subscribe<Events.Game.LevelFinishedEvent>(SaveLevel);
    }

    private void SaveLevel(LevelFinishedEvent _)
    {
        currentLevel++;
        PlayerPrefs.SetInt(Level, currentLevel);
        PlayerPrefs.Save();
    }

    private void SaveMoney(MoneyEarnedEvent eventData)
    {
        currentMoney += eventData.MoneyEarned;
        PlayerPrefs.SetInt(Money, currentMoney);
        PlayerPrefs.Save();
    }

    private void Start()
    {
        LoadMoney();
        LoadLevel();
    }

    private void LoadLevel()
    {
        currentLevel = PlayerPrefs.GetInt(Level, 1);
        EventDispatcher.Instance.Dispatch(new Events.Storage.LevelLoadedEvent(currentLevel));
    }

    private void LoadMoney()
    {
        currentMoney = PlayerPrefs.GetInt(Money, 0);
        EventDispatcher.Instance.Dispatch(new Events.Storage.MoneyLoadedEvent(currentMoney));
    }
}
