﻿using UnityEngine;

[System.Serializable]
public class FloatRange : Range<float>
{
    public FloatRange(float min, float max) : base(min, max) { }


    public override Range<float> MultipliedBy(float multiplier)
    {
        return new FloatRange(Min * multiplier, Max * multiplier);
    }

    public override Range<float> Sum(Range<float> range)
    {
        return new FloatRange(Min + range.Min, Max + range.Max);
    }


    public override float Random() => UnityEngine.Random.Range(Min, Max);

    public override float Clamp(float valueToClamp)
    {
        if (valueToClamp > Max)
        {
            return Max;
        }
        else if (valueToClamp < Min)
        {
            return Min;
        }
        else
        {
            return valueToClamp;
        }
    }

    public override float LerpUnclamped(float state)
    {
        return Min + (Max - Min) * state;
    }

    public override float PingPong(float t)
    {
        return Mathf.PingPong(t - Min, Max - Min) + Min;
    }
}
