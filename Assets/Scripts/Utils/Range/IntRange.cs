﻿using UnityEngine;

[System.Serializable]
public class IntRange : Range<int>
{
    public IntRange(int min, int max) : base(min, max) { }


    public override Range<int> MultipliedBy(float multiplier)
    {
        return new IntRange(Mathf.RoundToInt(Min * multiplier), Mathf.RoundToInt(Max * multiplier));
    }

    public override Range<int> Sum(Range<int> range)
    {
        return new IntRange(Min + range.Min, Max + range.Max);
    }


    public override int Random() => UnityEngine.Random.Range(Min, Max);

    public override int Clamp(int valueToClamp)
    {
        return Mathf.Clamp(valueToClamp, Min, Max);
    }

    public override int LerpUnclamped(float state)
    {
        var raw = Min + (Max - Min) * state;
        return Mathf.RoundToInt(raw);
    }

    public override int PingPong(int t)
    {
        var pingPonged = Mathf.PingPong(t - Min, Max - Min);
        return Mathf.RoundToInt(pingPonged + Min);
    }
}
