﻿[System.Serializable]
public abstract class Range<T>
{
    public T Min;
    public T Max;

    public Range(T min, T max)
    {
        Min = min;
        Max = max;
    }


    public abstract Range<T> MultipliedBy(float multiplier);

    public abstract Range<T> Sum(Range<T> range);

    public abstract T Random();

    public abstract T Clamp(T valueToClamp);

    public abstract T LerpUnclamped(float state);

    public T Lerp(float state)
    {
        return LerpUnclamped(UnityEngine.Mathf.Clamp01(state));
    }

    public abstract T PingPong(T t);
}
