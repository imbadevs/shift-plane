﻿using System;
using System.Collections.Generic;


namespace GlobalEventSystem
{
    internal abstract class GlobalEvent { }

    internal class GlobalEvent<T> : GlobalEvent
    {
        private Dictionary<SubscriberID<GlobalEvent<T>>, Action<T>> subscribers = new Dictionary<SubscriberID<GlobalEvent<T>>, Action<T>>();
        private int currentID;

        private bool locked;
        private Queue<Action> unsubs = new Queue<Action>();


        internal SubscriberID<GlobalEvent<T>> Subscribe(Action<T> action)
        {
            var id = new SubscriberID<GlobalEvent<T>>(currentID++);
            subscribers.Add(id, action);
            return id;
        }

        internal void Unsubscribe(SubscriberID<GlobalEvent<T>> id)
        {
            if (locked)
            {
                unsubs.Enqueue(() => subscribers.Remove(id));
                return;
            }
            subscribers.Remove(id);
        }

        internal void UnsubscribeAll()
        {
            if (locked)
            {
                unsubs.Enqueue(subscribers.Clear);
                return;
            }
            subscribers.Clear();
        }

        internal void Dispatch(T eventData)
        {
            locked = true;
            foreach (var action in subscribers.Values)
            {
                action(eventData);
            }
            locked = false;

            foreach (var action in unsubs)
            {
                action();
            }
        }
    }
}

