﻿using System;
using System.Collections.Generic;


namespace GlobalEventSystem
{
    class EventDispatcher
    {
        private static EventDispatcher instance;
        public static EventDispatcher Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventDispatcher();
                }
                return instance;
            }
        }


        private List<GlobalEvent> events = new List<GlobalEvent>();

        private GlobalEvent<T> GetEvent<T>()
        {
            var globalEvent = (GlobalEvent<T>)events.Find(d => d is GlobalEvent<T>);
            if (globalEvent == null)
            {
                globalEvent = new GlobalEvent<T>();
                events.Add(globalEvent);
            }
            return globalEvent;
        }


        public SubscriberID<GlobalEvent<T>> Subscribe<T>(Action<T> action)
        {
            var globalEvent = GetEvent<T>();
            return globalEvent.Subscribe(action);
        }


        public void Unsubscribe<T>(SubscriberID<GlobalEvent<T>> subscriberID)
        {
            var globalEvent = GetEvent<T>();
            globalEvent.Unsubscribe(subscriberID);
        }

        public void Unsubscribe<T>()
        {
            var globalEvent = GetEvent<T>();
            globalEvent.UnsubscribeAll();
        }

        public void UnsubscribeAll()
        {
            events.Clear();
        }

        public void Dispatch<T>(T data)
        {
            var globalEvent = GetEvent<T>();
            globalEvent.Dispatch(data);
        }
    }
}

