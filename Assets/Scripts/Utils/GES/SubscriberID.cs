﻿namespace GlobalEventSystem
{
    class SubscriberID<E>
    {
        public readonly int ID;
        public SubscriberID(int id) => ID = id;

        public override bool Equals(object obj)
        {
            return obj is SubscriberID<E> id &&
                   ID == id.ID;
        }

        public override int GetHashCode()
        {
            return 1213502048 + ID.GetHashCode();
        }
    }
}

