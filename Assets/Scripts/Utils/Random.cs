﻿namespace Utils
{
    public static class Random
    {
        public static bool HappensWith(float probability)
        {
            return probability != 0 && UnityEngine.Random.value <= probability;
        }

        public static bool Bool()
        {
            return UnityEngine.Random.Range(0, 2) == 1;
        }

        public static int PickRandomIndex<T>(this T[] array) => UnityEngine.Random.Range(0, array.Length);
        public static T PickRandom<T>(this T[] array) => array[array.PickRandomIndex()];


        public static T RandomIn<T>(Range<T> range) => range.Random();
    }
}
