﻿using System;

namespace Utils
{
    public static class Math
    {
        public static (T, T) MinMax<T>(T a, T b) where T : IComparable => a.CompareTo(b) > 0 ? (b, a) : (a, b);
    }
}
