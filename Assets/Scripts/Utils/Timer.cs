﻿using System;
using System.Collections;
using UnityEngine;

namespace Utils
{
    public class Timer
    {
        private const float One = 1f;

        public float Start { get; private set; }
        public float Duration { get; private set; }
        public Action<float> Action { get; private set; }
        public IEnumerator Period { get; private set; }
        public float End { get => Start + Duration; }


        private Timer() { }


        public static Timer WithStartAndDuration(float start, float duration)
        {
            return new Timer() { Start = start, Duration = duration };
        }

        public static Timer WithStartAndEnd(float start, float end)
        {
            return new Timer() { Start = start, Duration = end - start };
        }

        public static Timer WithDurationAndEnd(float duration, float end)
        {
            return new Timer() { Start = end - duration, Duration = duration };
        }


        public Timer Repeat(Action<float> action)
        {
            Action = action;
            return this;
        }

        public Timer Every(IEnumerator period)
        {
            Period = period;
            return this;
        }

        public IEnumerator CreateCoroutine()
        {
            yield return new WaitForSeconds(Start - Time.time);
            float state;
            while ((state = (Time.time - Start) / Duration) < One)
            {
                Action(state);
                yield return Period;
            }
            Action(One);
        }
    }
}
