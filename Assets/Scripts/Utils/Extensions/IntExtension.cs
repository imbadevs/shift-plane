﻿public static class IntExtension
{
    public static int ClampWithin(this int value, IntRange range)
    {
        return range.Clamp(value);
    }
}
