﻿public static class FloatExtension
{
    public static float ClampWithin(this float value, FloatRange range)
    {
        return range.Clamp(value);
    }
}
