﻿using System;

public static class CollectionsExtension
{
    public static bool FindExplicit<T>(this T[] ts, Predicate<T> predicate, out T found)
    {
        for (int i = 0, end = ts.Length; i < end; i++)
        {
            if (predicate(ts[i]))
            {
                found = ts[i];
                return true;
            }
        }

        found = default;
        return false;
    }

    public static T Last<T>(this T[] ts) => ts[ts.Length - 1];

    public static int LastIndex<T>(this T[] ts) => ts.Length - 1;

    public static R[] Map<X, R>(this X[] xs, Func<X, R> f)
    {
        var results = new R[xs.Length];
        for (int i = 0; i < xs.Length; i++)
        {
            results[i] = f(xs[i]);
        }
        return results;
    }

    public static void Map_<X>(this X[] xs, Action<X> action)
    {
        for (int i = 0; i < xs.Length; i++)
        {
            action(xs[i]);
        }
    }

    public static A Foldl<X, A>(this X[] xs, Func<A, X, A> f, A accumulator)
    {
        for (int i = 0, len = xs.Length; i < len; i++)
        {
            accumulator = f(accumulator, xs[i]);
        }
        return accumulator;
    }

    public static X Foldl1<X>(this X[] xs, Func<X, X, X> f)
    {
        if (xs.Length == 0)
            throw new ArgumentException("Empty list", nameof(xs));

        if (xs.Length == 1)
            return xs[0];

        X accumulator = f(xs[0], xs[1]);
        for (int i = 2, len = xs.Length; i < len; i++)
        {
            accumulator = f(accumulator, xs[i]);
        }
        return accumulator;
    }


    public static void AsZipper<X>(this X[] xs, Action<X, X> action, int step = 1)
    {
        const int window = 2;
        for (int i = window - 1, len = xs.Length; i < len; i += step)
        {
            action(xs[i - 1], xs[i]);
        }
    }
}