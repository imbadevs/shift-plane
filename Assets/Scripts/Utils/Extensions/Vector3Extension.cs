﻿using UnityEngine;

public static class Vector3Extension
{
    // Do not create new ones because
    // struct is already being duplicated during function call.

    public static Vector3 With(this Vector3 original, float? x = null, float? y = null, float? z = null)
    {
        if (x.HasValue) original.x = x.Value;
        if (y.HasValue) original.y = y.Value;
        if (z.HasValue) original.z = z.Value;
        return original;
    }

    public static Vector3 Flatened(this Vector3 original, float ground = 0)
    {
        original.y = ground;
        original.Normalize();
        return original;
    }

    public static void Add(this ref Vector3 original, float dx = 0, float dy = 0, float dz = 0)
    {
        original.x += dx;
        original.y += dy;
        original.z += dz;
    }

    public static Vector3 Added(this Vector3 original, float dx = 0, float dy = 0, float dz = 0)
    {
        original.x += dx;
        original.y += dy;
        original.z += dz;
        return original;
    }

    public static void Multiply(this ref Vector3 original, float mx = 1, float my = 1, float mz = 1)
    {
        original.x *= mx;
        original.y *= my;
        original.z *= mz;
    }

    public static Vector3 Multiplied(this Vector3 original, float mx = 1, float my = 1, float mz = 1)
    {
        original.x *= mx;
        original.y *= my;
        original.z *= mz;
        return original;
    }
}
