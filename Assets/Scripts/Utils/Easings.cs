﻿using UnityEngine;

namespace Utils
{
    public static class Easings
    {
        public static float EaseOut(int power, float t)
        {
            switch (power)
            {
                case 2: return 1 - (--t) * t;
                case 3: return 1 + (--t) * t * t;
                case 4: return 1 - (--t) * t * t * t;
                case 5: return 1 + (--t) * t * t * t * t;
                case 6: return 1 - (--t) * t * t * t * t * t;
                default: throw new System.ArgumentException("Power has to be in range [2, 6]");
            }
        }

        public static float EaseIn(int power, float t)
        {
            switch (power)
            {
                case 2: return t * t;
                case 3: return t * t * t;
                case 4: return t * t * t * t;
                case 5: return t * t * t * t * t;
                case 6: return t * t * t * t * t * t;
                default: throw new System.ArgumentException("Power has to be in range [2, 6]");
            }
        }

        public static float EaseInOut(int power, float t)
        {
            switch (power)
            {
                case 2: return t < 0.5f ? 2 * t * t : 1 - 2 * (--t) * t;
                case 3: return t < 0.5f ? 4 * t * t * t : 1 + 4 * (--t) * t * t;
                case 4: return t < 0.5f ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
                case 5: return t < 0.5f ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
                case 6: return t < 0.5f ? 32 * t * t * t * t * t * t : 1 - 32 * (--t) * t * t * t * t * t;
                default: throw new System.ArgumentException("Power has to be in range [2, 6]");
            }
        }
    }
}
