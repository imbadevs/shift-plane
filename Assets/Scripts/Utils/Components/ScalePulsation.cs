﻿using System;
using UnityEngine;
using Utils;

public class ScalePulsation : MonoBehaviour
{
    [Header("Axes")]
    [SerializeField] private bool x;
    [SerializeField] private bool y;
    [SerializeField] private bool z;

    [Header("Settings")]
    [SerializeField] private FloatRange scaleRange = new FloatRange(0f, 1f);
    [SerializeField] private float halfPeriod = 1f;
    [SerializeField] [Range(-1, 1)] private int direction = 1;
    [SerializeField] private EasingKind easingKind = EasingKind.Linear;
    [SerializeField] [Range(2, 6)] private int easingPower = 2;

    private float startTime;
    private Func<float, float> easing;

    private void Start()
    {
        startTime = Time.time + 0.5f * halfPeriod - direction * 0.5f * halfPeriod;
        switch (easingKind)
        {
            case EasingKind.Linear:
                easing = t => t;
                break;
            case EasingKind.EaseIn:
                easing = t => Easings.EaseIn(easingPower, t);
                break;
            case EasingKind.EaseOut:
                easing = t => Easings.EaseOut(easingPower, t);
                break;
            case EasingKind.EaseInOut:
                easing = t => Easings.EaseInOut(easingPower, t);
                break;
            default: throw new NotImplementedException();
        };
    }

    private void Update()
    {
        var state = Mathf.PingPong(Time.time - startTime, halfPeriod) / halfPeriod;
        var scaleFactor = scaleRange.Lerp(easing(state));
        transform.localScale = transform.localScale.With(x ? (float?)scaleFactor : null, y ? (float?)scaleFactor : null, z ? (float?)scaleFactor : null);
    }

    private void OnValidate()
    {
        if (direction == 0)
        {
            direction = 1;
            Debug.LogError("Allowed values for *direction* are -1 and 1. Set *direction* to 1.");
        }
    }
}
