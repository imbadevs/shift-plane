﻿using UnityEngine;

public class DoNotDestroyOnLoadTag : MonoBehaviour
{
    private void Awake()
    {
        var nonDestroyableObjects = FindObjectsOfType<DoNotDestroyOnLoadTag>();
        var occurences = 0;
        for (int i = 0; occurences <= 1 && i < nonDestroyableObjects.Length; ++i)
        {
            if (nonDestroyableObjects[i].gameObject.name == gameObject.name)
            {
                ++occurences;
            }
        }

        if (occurences != 1)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
