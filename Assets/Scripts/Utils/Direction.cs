﻿[System.Serializable]
public enum Direction : short
{
    Left = -1,
    Right = 1
}