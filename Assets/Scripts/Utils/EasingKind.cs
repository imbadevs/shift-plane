﻿namespace Utils
{
    [System.Serializable]
    enum EasingKind { Linear, EaseIn, EaseOut, EaseInOut }
}
