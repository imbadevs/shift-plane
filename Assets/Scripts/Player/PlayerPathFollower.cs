﻿using Events.Game;
using Events.LevelGeneration;

using GlobalEventSystem;

using UnityEngine;


class PlayerPathFollower : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    public float Speed { get => speed; }

    [SerializeField] private float turningSpeed = 45f;


    private Path path;

    private float movedDistance = 0f;


    private void Awake()
    {
        if (enabled)
        {
            enabled = false;
            EventDispatcher.Instance.Subscribe<Events.Game.GameStartEvent>(_ => enabled = true);
            EventDispatcher.Instance.Subscribe<Events.Game.PlayerKilledEvent>(OnPlayerKilled);
            EventDispatcher.Instance.Subscribe<Events.LevelGeneration.PlanePathCreatedEvent>(SavePath);
        }
    }


    private void SavePath(PlanePathCreatedEvent data)
    {
        path = data.Path;
    }


    private void Update()
    {
        Move();
    }


    private void Move()
    {
        if (movedDistance >= path.Length)
        {
            EventDispatcher.Instance.Dispatch(new Events.Game.FinishReachedEvent());
        }
        else
        {
            movedDistance += Time.deltaTime * speed;
            AuxMove(movedDistance);
        }
    }

    private void AuxMove(float distance)
    {
        var nextPoint = path.FollowPath(distance);

        transform.position = nextPoint.Position.With(y: transform.position.y);

        transform.forward = Vector3.RotateTowards(transform.forward, nextPoint.Forward, Mathf.Deg2Rad * turningSpeed, 0);

        EventDispatcher.Instance.Dispatch(new Events.Game.PlayerMovedEvent(distance, path.Length));
    }


    private void OnPlayerKilled(PlayerKilledEvent _)
    {
        enabled = false;
    }
}