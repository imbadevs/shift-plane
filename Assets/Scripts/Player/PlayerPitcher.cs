﻿using UnityEngine;

public class PlayerPitcher : MonoBehaviour
{
    [SerializeField] private IVerticalMover mover;

    [SerializeField] private Transform plane;

    [SerializeField] private float maxPitchAngle = 35f;
    [SerializeField] private float sensitivity = 0.5f;
    [SerializeField] private float pitchSpeed = 1f;
    [SerializeField] private float pitchReturnSpeed = 0.5f;

    private float _pitchState;
    private float PitchState
    {
        get => _pitchState;
        set => _pitchState = Mathf.Clamp(value, -1f, 1f);
    }

    private void Update()
    {
        Pitch(mover.Delta);
    }


    private void Pitch(float delta)
    {
        if (delta > 0)
        {
            PitchState -= delta * sensitivity * pitchSpeed * Time.deltaTime;
        }
        else if (delta < 0)
        {
            PitchState -= delta * sensitivity * pitchSpeed * Time.deltaTime;
        }
        else
        {
            PitchState = Mathf.MoveTowards(PitchState, 0f, pitchReturnSpeed * Time.deltaTime);
        }

        float nextXRotation = maxPitchAngle * PitchState;
        plane.transform.localEulerAngles = plane.transform.localEulerAngles.With(x: nextXRotation);
    }
}
