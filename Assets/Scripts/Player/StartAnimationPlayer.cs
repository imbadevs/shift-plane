﻿using UnityEngine;

public class StartAnimationPlayer : MonoBehaviour
{
    [SerializeField] private Animator startAnimator;


    private void Awake()
    {
        GlobalEventSystem.EventDispatcher.Instance.Subscribe<Events.Game.PreStartPlayerAnimationEvent>(PlayAnimation);
    }

    private void PlayAnimation(Events.Game.PreStartPlayerAnimationEvent _)
    {
        //startAnimator.SetTrigger("Start");
        PostAnimationGameStarter();
    }

    // Called from animation.
    private void PostAnimationGameStarter()
    {
        startAnimator.enabled = false;
        GlobalEventSystem.EventDispatcher.Instance.Dispatch(new Events.Game.GameStartEvent());
    }
}
