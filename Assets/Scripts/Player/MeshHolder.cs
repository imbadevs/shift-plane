﻿using UnityEngine;

public class MeshHolder : MonoBehaviour
{
    [SerializeField] private Transform planeRoot;
    [SerializeField] private int currentPlane = 0;
    [SerializeField] private GameObject[] planes;

    private void Awake()
    {
        Instantiate(planes[currentPlane], planeRoot);
    }
}
