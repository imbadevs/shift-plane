﻿using System.Collections;

using GlobalEventSystem;

using UnityEngine;

class PlayerFinisher : MonoBehaviour
{
    [SerializeField] private Cinemachine.CinemachineVirtualCamera followingCamera;
    [SerializeField] private IVerticalMover verticalMover;
    [SerializeField] private PlayerPathFollower curveFollower;
    [SerializeField] private PlayerRoller playerRoller;

    [Header("Animation")]
    [SerializeField] private float angularSpeed = 90f;
    [SerializeField] private float angularAccelerationMultiplier = 2f;
    [SerializeField] private float risingSpeed = 50f;
    [SerializeField] private float accelerationMultiplier = 2f;
    [SerializeField] private float animationDuration = 2f;

    [Header("Fireworks")]
    [SerializeField] private Fireworks fireworksPrefab;
    [SerializeField] private float fireworksRingRadius = 4f;
    [SerializeField] private int radialFireworksNumber = 12;
    [SerializeField] private float fireworksSpawnPeriod = 0.3f;


    private void Awake()
    {
        EventDispatcher.Instance.Subscribe<Events.Game.FinishReachedEvent>(_ => Finish());
    }


    public void Finish()
    {
        verticalMover.enabled = false;
        playerRoller.enabled = false;
        curveFollower.enabled = false;
        followingCamera.enabled = false;
        StartCoroutine(FinishAnimation());
    }

    private IEnumerator FinishAnimation()
    {
        RadialFireworks();

        var speed = curveFollower.Speed;
        var duration = animationDuration;
        var spaceForward = Vector3.forward;
        var spaceUp = Vector3.up;

        var fireworksCoolDown = fireworksSpawnPeriod;

        while (duration > 0)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
            transform.rotation *= Quaternion.AngleAxis(angularSpeed * Time.deltaTime, spaceForward);
            transform.RotateAround(transform.position, Vector3.Cross(transform.forward, spaceUp), risingSpeed * Time.deltaTime);
            speed += speed * accelerationMultiplier * Time.deltaTime;
            angularSpeed += angularSpeed * angularAccelerationMultiplier * Time.deltaTime;

            if (fireworksCoolDown <= 0)
            {
                var position = transform.position + transform.up * fireworksRingRadius;
                var fireworks = Instantiate(fireworksPrefab, position, Quaternion.identity);
                fireworks.Play();
                fireworksCoolDown = fireworksSpawnPeriod;
            }

            yield return null;
            duration -= Time.deltaTime;
            fireworksCoolDown -= Time.deltaTime;
        }

        PostAnimationFinishing();
    }

    private void RadialFireworks()
    {
        for (var i = 0; i < radialFireworksNumber; i++)
        {
            var angle = i * 360f / radialFireworksNumber;
            var offset = Quaternion.AngleAxis(angle, transform.forward) * Vector3.up * fireworksRingRadius;
            var position = transform.position + offset;
            var fireworks = Instantiate(fireworksPrefab, position, Quaternion.identity);
            fireworks.Play(Random.Range(0f, 0.25f));
        }
    }

    private void PostAnimationFinishing()
    {
        GameManager.Instance.Finish();
    }
}
