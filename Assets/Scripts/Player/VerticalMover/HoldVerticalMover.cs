﻿using Events.Game;

using UnityEngine;

class HoldVerticalMover : IVerticalMover
{
    [SerializeField] private float downwardAcceleration = -20f;
    [SerializeField] private float upwardAcceleration = 20f;
    [SerializeField] private float maxSpeed = 20f;


    private IHoldInputController inputController;

    private float speed;


    protected override void InitInputController()
    {
#if UNITY_EDITOR
        inputController = new Dev_EditorHoldInputController();
#else
        inputController = new TouchHoldInputController();
#endif
    }


    protected override void Move()
    {
        var deltaTime = Time.deltaTime;
        var acceleration = inputController.Input ? upwardAcceleration : downwardAcceleration;

        if (transform.position.y == minY || transform.position.y == maxY)
        {
            speed = 0f;
        }
        speed = Mathf.Clamp(speed + acceleration * deltaTime, -maxSpeed, maxSpeed);

        var rawDelta = speed * deltaTime;
        var prevY = transform.position.y;
        var newY = prevY + rawDelta;

        transform.position = transform.position.With(y: newY);
    }


    protected override void OnPlayerKilled(PlayerKilledEvent _)
    {
        enabled = false;
    }
}
