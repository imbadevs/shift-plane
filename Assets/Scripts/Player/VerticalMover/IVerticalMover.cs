﻿using GlobalEventSystem;

using UnityEngine;

abstract class IVerticalMover : MonoBehaviour
{
    public float Delta { get; private set; }

    protected float minY;
    protected float maxY;

    protected abstract void InitInputController();
    protected abstract void Move();

    protected abstract void OnPlayerKilled(Events.Game.PlayerKilledEvent _);


    private bool isMoving = false;


    private void Awake()
    {
        InitInputController();
        EventDispatcher.Instance.Subscribe<Events.LevelGeneration.MapHeightDeterminedEvent>((data) => { minY = data.MinY; maxY = data.MaxY; });
        EventDispatcher.Instance.Subscribe<Events.Game.PlayerKilledEvent>(OnPlayerKilled);
        EventDispatcher.Instance.Subscribe<Events.Game.GameStartEvent>(_ => isMoving = true);
    }


    private void Update()
    {
        if (isMoving)
        {
            var y0 = transform.position.y;
            Move();
            transform.position = transform.position.With(y: Mathf.Clamp(transform.position.y, minY, maxY));
            var y1 = transform.position.y;
            Delta = y1 - y0;
        }
    }
}
