﻿class SlideVerticalMover : IVerticalMover
{
    private ISlideInputController controller;


    protected override void InitInputController()
    {
#if UNITY_EDITOR
        // Use special editor controller during development.
        controller = new Dev_EditorSlideInputController();
#else
        // Use normal touch controller in release.
        controller = new TouchSlideInputController();
#endif
    }

    protected override void Move()
    {
        var newPos = transform.position;
        newPos.y += controller.GetInput;
        transform.position = newPos;
    }


    protected override void OnPlayerKilled(Events.Game.PlayerKilledEvent _)
    {
        enabled = false;
    }
}
