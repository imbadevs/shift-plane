﻿using GlobalEventSystem;
using UnityEngine;

public class PlayerRoller : MonoBehaviour
{
    [SerializeField] private Transform plane;
    [SerializeField] private float sensitivity = 0.1f;
    [SerializeField] private float maxAngle = 45f;
    [SerializeField] private float speed = 30f;

    private Vector3 prevForward = Vector3.forward;
    private float prevRollAngle;


    private void Awake()
    {
        if (enabled)
        {
            enabled = false;
            EventDispatcher.Instance.Subscribe<Events.Game.GameStartEvent>(_ => enabled = true);
            EventDispatcher.Instance.Subscribe<Events.Game.PlayerKilledEvent>(_ => enabled = false);
        }
    }


    private void Update()
    {
        Roll();
    }


    private void Roll()
    {
        var forward = transform.forward;
        var deltaAngle = -Vector3.SignedAngle(prevForward, forward, Vector3.up);
        var multiplier = Mathf.Clamp(deltaAngle * sensitivity, -1f, 1f);

        var desiredRollAngle = multiplier * maxAngle;
        var rollAngle = Mathf.MoveTowardsAngle(prevRollAngle, desiredRollAngle, speed * Time.deltaTime);

        plane.localRotation = Quaternion.AngleAxis(rollAngle, Vector3.forward);

        prevForward = forward;
        prevRollAngle = rollAngle;
    }
}
