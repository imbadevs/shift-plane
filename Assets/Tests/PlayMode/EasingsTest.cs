﻿using Curves;
using NUnit.Framework;

using Unity.PerformanceTesting;

using UnityEngine;
using Utils;

namespace PlayModeTests
{
    public class EasingsTest
    {
        private const int warmup = 50;
        private const int measures = 100;
        private const int iterations = 100;
        private const float t = 0.5f;

        [Test, Performance]
        public void EasingsPowerTest(/*[Values(2, 3, 4, 5, 6)] int power*/)
        {
            Measure.Method(() => Easings.EaseInOut(2, t)).SampleGroup("Switch").WarmupCount(warmup).MeasurementCount(measures).IterationsPerMeasurement(iterations).Run();
        }
    }
}
